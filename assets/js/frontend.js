$(function(){
	
	var i = 0,
	featured = $('.home-featured .property');
	
	$( "#search_in, #search_out" ).datepicker({
			minDate: 0,
			dateFormat: 'dd-mm-yy',
			prevText: '←',
			nextText: '→'
	});
	
	if(featured.length){
	
		$(featured).click(function() {
			$(location).attr('href','/properties/view/'+$(this).attr('property'));
		});
		
		$('.home-featured .prev').click(function() {
			i = (i==0) ? featured.length : i--;
			showProperty();
			//clearInterval(int);
		});
		
		$('.home-featured .next').click(function() {
			i = (i==featured.length) ? 0 : i++;
			showProperty();
			//clearInterval(int);
		});

		function showProperty () {
			featured.hide()
			.filter(function (index) { return index == i % featured.length; })
			.fadeIn('slow'); 

			i++;
		}; 

		var int = setInterval(function () {
			showProperty();
		}, 10 * 1000);
		
		showProperty();
    
    }
	
	//$('img').mouseover(function() {
	//	$(this).fadeTo('slow','0.5',function(){
	//		$(this).fadeTo('slow','1.0')
	//	});
	//});
	
	$('.home-boxes .why').click(function() {
		$(location).attr('href','/why-south4rent');
	});
	
	$('.home-boxes .list').click(function() {
		$(location).attr('href','/list-your-property');
	});
	
	$('.home-boxes .cityguide').click(function() {
		$(location).attr('href','/cityguide');
	});

	$('.home-boxes .contact-us').click(function() {
		$(location).attr('href','/contact-us');
	});

	$('.home-boxes .cityguide.brasil').click(function() {
		$(location).attr('href','/cityguide/saopaulo');
	});

	$('.home-boxes .cityguide.chile').click(function() {
		$(location).attr('href','/cityguide/santiago');
	});

	$('.city-selection .santiago-chile').click(function() {
		$(location).attr('href','/chile');
	});

	$('.city-selection .sanpablo-brasil').click(function() {
		$(location).attr('href','/brasil');
	});
	
	$('.search-box .search').click(function() {
		var search_where = $('#search_where').val() ? $('#search_where').val() : 0;
		var search_in = $('#search_in').val() ? $('#search_in').val() : 0;
		var search_out = $('#search_out').val() ? $('#search_out').val() : 0;
		var search_rooms = $('#search_rooms').val() ? $('#search_rooms').val() : 0;
		
		search_where ? search_where : 0;
		search_in ? search_in : 0;
		search_out ? search_out : 0;
		search_rooms ? search_rooms : 0;
		
		//$(location).attr('href','/properties/search/'+search_where+'/'+search_in+'/'+search_out+'/'+search_rooms);
		$(location).attr('href','/properties/search/?w='+search_where+'&i='+search_in+'&o='+search_out+'&r='+search_rooms);
	});
	
	// -->
	
	$('.calendar_select').click(function() {
		$('#calendar').fadeIn('fast');
		$('#photos').hide();
		$('#map').hide();
	});
	
	$('.photos_select').click(function() {
		$('#calendar').hide();
		$('#photos').fadeIn('fast');
		$('#map').hide();
	});
	
	$('.map_select').click(function() {
		$('#calendar').hide();
		$('#photos').hide();
		$('#map').fadeIn('fast');
		initialize();
	});
	
	/** TODO: Currency Fix. Currently Disabled.
	$('.currency_select').click(function() {
		$(this).hide();
		$('.currency_rs').show();
		$('.currency_usd').show();
	});
	
	$('.currency_rs').click(function() {
		$('.currency_convert_rs').show();
		$('.currency_convert_usd').hide();
	});
	
	$('.currency_usd').click(function() {
		$('.currency_convert_usd').show();
		$('.currency_convert_rs').hide();
	});
	*/
	
	$('#photos .thumbs ul li').click(function() {
		var id = $(this).attr('data-photo');
		var order = $(this).attr('data-order');
		$('#photos .full img').attr('src','/upload/properties/full/' + id +'.jpg');
		$('#photos .full img').attr('data-actual',order);
	});
	
	$('#photos .full .prev').click(function() {
		order = parseInt($('#photos .full img').attr('data-actual')) - 1;
		if(order>0){
			thumb = $('#photos .thumbs ul li[data-order="'+order+'"]');
			var id = $(thumb).attr('data-photo');
			$('#photos .full img').attr('src','/upload/properties/full/' + id +'.jpg');
			$('#photos .full img').attr('data-actual',order);
		}
	});
		
	$('#photos .full .next').click(function() {
		order = parseInt($('#photos .full img').attr('data-actual')) + 1;
		thumb = $('#photos .thumbs ul li[data-order="'+order+'"]');
		if(thumb.length==0){
			order = 1;
			thumb = $('#photos .thumbs ul li[data-order="'+order+'"]');
		}
		var id = $(thumb).attr('data-photo');
		$('#photos .full img').attr('src','/upload/properties/full/' + id +'.jpg');
		$('#photos .full img').attr('data-actual',order);
	});
	
	$('.search-results .properties .property').click(function() {
		$(location).attr('href','/properties/view/'+$(this).attr('property'));
	});
	
	$('.search-box .title').click(function() {
		$(location).attr('href','/properties/search');
	});
	
	//Assign to those input elements that have 'placeholder' attribute
	$('input[placeholder]').each(function(){  
        
		var input = $(this);        
		$(input).val(input.attr('placeholder'));
		$(input).focus(function(){
			if (input.val() == input.attr('placeholder')) {
				input.val('');
			}
		});
        
		$(input).blur(function(){
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
			}
		});
	});
	
	
}); 

function pin(lat,lng,i) {

	var latlng = new google.maps.LatLng(
		parseFloat(lat),
		parseFloat(lng)
	);
	
	map.setCenter(latlng);
	var marker = new google.maps.Marker({
		map: map, 
		position: latlng,
		animation: google.maps.Animation.DROP,
	});
	
	bounds.extend(latlng);
	map.fitBounds(bounds);
			
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
			$(location).attr('href','/properties/view/'+locations[i][0]);
		}
	})(marker, i));
}