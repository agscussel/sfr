$(function(){

	$('.dropdown-toggle').dropdown();
	
	$(".from_date, .to_date").datepicker({
			minDate: 0,
			dateFormat: 'yy-mm-dd',
			prevText: '←',
			nextText: '→'
	});
	
    $('#myTab a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
    })
    
	$('#myTab a:first').tab('show');
	
    $('#map_refresh').click(function (e) {
		$('#google_maps').hide();
		$('#geocoded_map').show();
    	var location = $('#location').val();
		codeAddress(location);
    })
    
    $('#signin').modal({
		keyboard: false,
		backdrop: false
	})
	
	$('#myTab a:first').tab('show');
	
    $('.navbar .nav li').click(function (e) {
    	$('.navbar .nav li').removeClass('active');
		$(this).addClass('active');
    })
        
    if($('#country_id').length){
		$('#country_id').change(function() {
			fetchCities($(this).val());
		});
		
		 //fetchCountries();
	}
	
	if($('#city_id').length){
		$('#city_id').change(function() {
			fetchNeighborhoods($(this).val());
		});
	}

});

function fetchCountries(){
	$.getJSON("/locations/countries/", function(result) {
		var countries = $("#country_id");
		countries.empty();
		$.each(result, function() {
			countries.append($("<option />").val(this.id).text(this.name));
		});
		fetchCities(countries.val());
	});
}
    
function fetchCities(country_id){
	$.getJSON("/locations/cities/"+country_id, function(result) {
		var cities = $("#city_id");
		var neighborhoods = $("#neighborhood_id");
		cities.empty();
		neighborhoods.empty();
		$.each(result, function() {
			cities.append($("<option />").val(this.id).text(this.name));
		});
		fetchNeighborhoods(cities.val());
	});
}
    
function fetchNeighborhoods(city_id){
	$.getJSON("/locations/neighborhoods/"+city_id, function(result) {
		var neighborhoods = $("#neighborhood_id");
		neighborhoods.empty();
		$.each(result, function() {
			neighborhoods.append($("<option />").val(this.id).text(this.name));
		});
	});
}

var geocoder;
var map;

function codeAddress(address) {
	//console.log(address);
	geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {			
			var myOptions = {
				zoom: 17,
				center: results[0].geometry.location,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			var marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
			});
			
			//console.log(results[0].geometry.location);             
	
			$('#google_lat').val(results[0].geometry.location.lat());
			$('#google_lng').val(results[0].geometry.location.lng());
			
			map = new google.maps.Map(document.getElementById('geocoded_map'), myOptions);
			marker.setMap(map);
			//google.maps.event.trigger(map, 'resize');
			
		} else {
			alert("Geocode was not successful for the following reason: " + status);
		}
    });
}
