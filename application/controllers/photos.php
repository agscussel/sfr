<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Photos extends MY_Frontend
{

	function __construct()
	{
		
		parent::__construct();	
        $this->load->library('image_lib');
	
	}

    public function index ($id)
    {

        $configs = array();
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/slide/$id.jpg", 'width' => 900, 'height' => 300, 'maintain_ratio' => FALSE);
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/full/$id.jpg", 'width' => 536, 'height' => 345, 'maintain_ratio' => FALSE);
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/prev/$id.jpg", 'width' => 260, 'height' => 180, 'maintain_ratio' => FALSE);
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/square/$id.jpg", 'width' => 50, 'height' => 50, 'maintain_ratio' => FALSE);

        foreach ($configs as $config) {
            $this->image_lib->thumb($config, FCPATH . 'upload/');
        }

        $data = array('images' => $configs);
       //print_r($data);
    }
}

/* End of file photos.php */
/* Location: ./application/controllers/photos.php */