<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Backend {

	public function index()
	{
		
		if($this->session->userdata('account'))
		{	
			redirect('/backend/properties');
		}
		else 
		{
			redirect('/backend/accounts/signin');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/backend/welcome.php */