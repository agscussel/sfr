<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Properties extends MY_Backend {

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Property_model');
		$this->load->model('Location_model');
		$this->load->model('Feature_model');
		$this->load->model('Photo_model');
		$this->load->model('Date_model');
		$this->load->model('Availability_model');
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		$this->require_login();
	
	}

	public function index()
	{
		
		$content_data = array(
			'properties' => $this->Property_model->all()
		);
		
		$data = array(
			'title' => 'South4Rent - Properties list',
			'content' => $this->load->view('backend/properties/list',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function create()
	{
	
		if($this->input->post())
		{
			$data = array(
				'name' => $this->input->post('name'),
				'title_es' => $this->input->post('title_es'),
				'title_en' => $this->input->post('title_en'),
				'title_pt' => $this->input->post('title_pt'),
				'description_es' => $this->input->post('description_es'),
				'description_en' => $this->input->post('description_en'),
				'description_pt' => $this->input->post('description_pt'),
				'country_id' => $this->input->post('country_id'),
				'city_id' => $this->input->post('city_id'),
				'neighborhood_id' => $this->input->post('neighborhood_id'),
				//'address_street' => $this->input->post('address_street'),
				//'address_number' => $this->input->post('address_number'),
				'location' => $this->input->post('location'),
				'property_type' => $this->input->post('property_type'),
				'rooms' => $this->input->post('rooms'),
				'bathrooms' => $this->input->post('bathrooms'),
				'max_guests' => $this->input->post('max_guests'),
				'daily_price' => $this->input->post('daily_price'),
				'weekly_price' => $this->input->post('weekly_price'),
				'monthly_price' => $this->input->post('monthly_price'),
				'minimum_stay_option' => $this->input->post('minimum_stay_option'),
				'minimum_stay_number' => $this->input->post('minimum_stay_number'),
				'indoor_space' => $this->input->post('indoor_space'),
				'outdoor_space' => $this->input->post('outdoor_space'),	
				'visible' => $this->input->post('visible'),
				'featured' => $this->input->post('featured'),			
				'created' => date('Y-m-d H:i:s',now()),
				'modified' => date('Y-m-d H:i:s',now())
			);
			
			$property_id = $this->Property_model->create($data);
			
			if($_FILES)
			{
				foreach($_FILES as $key => $value)
				{
					if (!empty($value['size']))
					{
						$photo_id = $this->upload_photo($key, $property_id);
						$this->Photo_model->makeDefault($photo_id, $property_id);
					}
				}
			}
			
			redirect('/backend/properties/edit/'.$property_id);
		}
		else
		{
		
			$content_data = array(
				'features' => $this->Feature_model->all()
			);
		
			$data = array(
				'title' => 'South4Rent - Create property',
				'content' => $this->load->view('backend/properties/create',$content_data,TRUE),
				'account' => $this->session->userdata('account'),
				'segment' => $this->segment

			);
			$this->load->view('backend/layout',$data);
		}
	}
	
	private function upload_photo($field, $property_id)
	{
	
		ini_set("memory_limit","128M");
		ini_set("upload_max_filesize","10M");
		ini_set("post_max_size","10M");

		$photo_id = $this->Photo_model->create(array('property_id' => $property_id));
				
		$config['file_name'] = $photo_id.'.jpg';
		$config['upload_path'] = './upload/properties/temp';
		$config['overwrite'] = TRUE;
		$config['allowed_types'] = 'jpg|jpeg|gif|png';
		$config['max_size'] = '2048';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->upload->initialize($config);	
		$this->upload->do_upload($field);
		
		$this->resize_photos($photo_id);
		return $photo_id;
	}

	public function edit($id)
	{
	
		if($this->input->post())
		{
			$data = array(
				'name' => $this->input->post('name'),
				'title_es' => $this->input->post('title_es'),
				'title_en' => $this->input->post('title_en'),
				'title_pt' => $this->input->post('title_pt'),
				'description_es' => $this->input->post('description_es'),
				'description_en' => $this->input->post('description_en'),
				'description_pt' => $this->input->post('description_pt'),
				'country_id' => $this->input->post('country_id'),
				'city_id' => $this->input->post('city_id'),
				'neighborhood_id' => $this->input->post('neighborhood_id'),
				//'address_street' => $this->input->post('address_street'),
				//'address_number' => $this->input->post('address_number'),
				'location' => $this->input->post('location'),
				'lat' => $this->input->post('lat'),
				'lng' => $this->input->post('lng'),
				'property_type' => $this->input->post('property_type'),
				'rooms' => $this->input->post('rooms'),
				'bathrooms' => $this->input->post('bathrooms'),
				'max_guests' => $this->input->post('max_guests'),
				'daily_price' => $this->input->post('daily_price'),
				'weekly_price' => $this->input->post('weekly_price'),
				'monthly_price' => $this->input->post('monthly_price'),
				'minimum_stay_option' => $this->input->post('minimum_stay_option'),
				'minimum_stay_number' => $this->input->post('minimum_stay_number'),
				'indoor_space' => $this->input->post('indoor_space'),
				'outdoor_space' => $this->input->post('outdoor_space'),	
				'visible' => $this->input->post('visible'),
				'featured' => $this->input->post('featured'),
				'modified' => date('Y-m-d H:i:s',now())
			);
			
			//$this->Date_model->clearDates($id);
			//
			//if($this->input->post('bad_dates_input'))
			//{
			//	$bad_dates = explode("-",$this->input->post('bad_dates_input'));
			//	foreach($bad_dates as $date)
			//	{
			//		$exploded_date = explode(',',$date);
			//		$year = $exploded_date[0];
			//		$month = $exploded_date[1];
			//		$day = $exploded_date[2];
			//		$status = $exploded_date[3];
			//		$dates_data = array(
			//			'year' => $year,
			//			'month' => $month,
			//			'day' => $day,
			//			'status' => $status,
			//			'property_id' => $id
			//		);
			//		$this->Date_model->create($dates_data);
			//	}
			//}
			
			$from_date = $this->input->post('from_date');
			$to_date = $this->input->post('to_date');
			$availability = $this->input->post('availability');
			if($from_date && $to_date)
			{
				$availibility_data = array(
					'from_date' => $from_date,
					'to_date' => $to_date,
					'status' => $availability,
					'property_id' => $id
				);
				$this->Availability_model->create($availibility_data);
			}
			
			$delete_date = $this->input->post('delete_date');
			if($delete_date)
			{
				foreach($delete_date as $date)
				{
					$this->Availability_model->delete($date);
				}
			}
			
			$this->Property_model->update($id, $data);
			$this->Photo_model->makeDefault($this->input->post('default'), $id);
			
			$this->Feature_model->clearPropertyFeatures($id);
			$checked_features = $this->input->post('feature_id');
			if($checked_features)
			{
				foreach($checked_features as $feature)
				{
					$this->Feature_model->assignFeature(array('property_id' => $id, 'feature_id' => $feature));
				}
			}
			
			$delete_photo = $this->input->post('delete_photo');
			if($delete_photo)
			{
				foreach($delete_photo as $photo)
				{
					$this->Photo_model->delete($photo);
				}
			}
			
			if($_FILES)
			{
				foreach($_FILES as $key => $value)
				{
					if (!empty($value['size']))
					{
						$this->upload_photo($key, $id);
					}
				}
			}
			
			redirect('/backend/properties/edit/'.$id);
		}
		else
		{
		
			$property = $this->Property_model->findByidBackend($id);
			if(!$property)
			{
				redirect('/backend/properties');
			}

		
			$countries = $this->Location_model->countries();
			foreach($countries as $country)
			{
				$countries_data[$country->id] = $country->name;
			}
		
			$cities = $this->Location_model->cities($property->country_id);
			foreach($cities as $city)
			{
				$cities_data[$city->id] = $city->name;
			}
			
			$neighborhoods = $this->Location_model->neighborhoods($property->city_id);
			foreach($neighborhoods as $neighborhood)
			{
				$neighborhoods_data[$neighborhood->id] = $neighborhood->name;
			}
			
			//$bad_dates = $this->Date_model->fetchByPropertyId($property->id);
		
			$content_data = array(
				'property' => $property,
				'features' => $this->Feature_model->checkedFeatures($id),
				'photos' => $this->Photo_model->findByPropertyId($id),
				'countries' => form_dropdown('country_id', $countries_data, $property->country_id,'id="country_id" class="span2"'),
				'cities' => form_dropdown('city_id', $cities_data, $property->city_id,'id="city_id" class="span2"'),
				'neighborhoods' => form_dropdown('neighborhood_id', $neighborhoods_data, $property->neighborhood_id,'id="neighborhood_id" class="span2"'),
				//'bad_dates' => $bad_dates,
				'availability' => $this->Availability_model->fetchByPropertyId($id)
			);
		
			$data = array(
				'title' => 'South4Rent - Edit property',
				'content' => $this->load->view('backend/properties/edit',$content_data,TRUE),
				'account' => $this->session->userdata('account'),
				'segment' => $this->segment

			);
			$this->load->view('backend/layout',$data);
		}
	}
	
    public function resize_photos($id)
    {

        $configs = array();
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/slide/$id.jpg", 'width' => 900, 'height' => 300, 'maintain_ratio' => FALSE);
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/full/$id.jpg", 'width' => 536, 'height' => 345, 'maintain_ratio' => FALSE);
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/prev/$id.jpg", 'width' => 260, 'height' => 180, 'maintain_ratio' => FALSE);
        $configs[] = array('source_image' => "/properties/temp/$id.jpg", 'new_image' => "/properties/square/$id.jpg", 'width' => 50, 'height' => 50, 'maintain_ratio' => FALSE);

        foreach ($configs as $config) {
            $this->image_lib->thumb($config, FCPATH . 'upload/');
        }

        $data = array('images' => $configs);
       //print_r($data);
    }
	
	public function delete($id)
	{
		
		$this->Property_model->delete($id);
		redirect('/backend/properties');
	}
	
}

/* End of file properties.php */
/* Location: ./application/controllers/backend/properties.php */