<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends MY_Backend {

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Account_model');
		$this->load->library('form_validation');

	}
	
	public function index()
	{
		
		$content_data = array(
			'accounts' => $this->Account_model->all()
		);
		
		$data = array(
			'title' => 'South4Rent - Accounts list',
			'content' => $this->load->view('backend/accounts/list',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function create()
	{
	
		if($this->input->post())
		{

			$array = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'password' => sha1($this->input->post('password')),
				'created' => date('Y-m-d H:i:s',now()),
				'modified' => date('Y-m-d H:i:s',now())
			);
			
			$create = $this->Account_model->create($array);
			
			redirect('/backend/accounts');
		}
		else
		{
		
			$data = array(
				'title' => 'South4Rent - Create account',
				'content' => $this->load->view('backend/accounts/create',FALSE,TRUE),
				'account' => $this->session->userdata('account'),
				'segment' => $this->segment

			);
			$this->load->view('backend/layout',$data);
		}
	}
	
	public function delete($id)
	{
		
		$this->Account_model->delete($id);
		redirect('/backend/accounts');
	}
	
	/* SIGN IN/OUT*/
	
	function signin()
	{
		
		$this->session->keep_flashdata('redir_to');
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data = array(
				'title' => 'South4Rent - backend'
			);
			$this->load->view('backend/signin',$data);
		}
		else
		{

			$email = $this->input->post('email');
			$password = sha1($this->input->post('password'));
			
			$account = $this->Account_model->authenticate($email,$password);
			
			if($account)
			{
				$this->session->set_userdata('account',$account);
				$redir_to = $this->session->flashdata('redir_to') ? $this->session->flashdata('redir_to') : '/backend/properties';
				redirect($redir_to);
			
			}
			else
			{
				$this->session->set_flashdata('error', 'Sign in error. Try again');
				redirect(current_url().'?signin_error');
			}
		}
	}


	public function signout()
	{
		
		$this->session->unset_userdata('account');
		$this->session->set_flashdata('success', 'Successfully signed out.');
		redirect('/backend/accounts/signin');
	}
}

/* End of file accounts.php */
/* Location: ./application/controllers/backend/accounts.php */