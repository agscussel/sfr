<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends MY_Backend {

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Location_model');

	}
	
	public function index()
	{
		$this->countries();
	}
	
	public function countries()
	{
		
		$content_data = array(
			'countries' => $this->Location_model->countries()
		);
		
		$data = array(
			'title' => 'South4Rent - Countries list',
			'content' => $this->load->view('backend/locations/countries',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function cities($country_id)
	{
		
		$content_data = array(
			'country' => $this->Location_model->country($country_id),
			'cities' => $this->Location_model->cities($country_id)
		);
		
		$data = array(
			'title' => 'South4Rent - Cities list',
			'content' => $this->load->view('backend/locations/cities',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function neighborhoods($city_id)
	{
	
		$city = $this->Location_model->city($city_id);
		$country = $this->Location_model->country_byCityId($city->id);
		
		$content_data = array(
			'city' => $city,
			'country' => $country,
			'neighborhoods' => $this->Location_model->neighborhoods($city_id)
		);
		
		$data = array(
			'title' => 'South4Rent - Neighborhoods list',
			'content' => $this->load->view('backend/locations/neighborhoods',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	// -->
	
	public function create_countries()
	{
	
		if($this->input->post())
		{
			$data = array(
				'name' => $this->input->post('name')
			);
			$this->Location_model->create_country($data);
			redirect('/backend/locations/countries');
		}

		$content_data = array();
		
		$data = array(
			'title' => 'South4Rent - Create Country',
			'content' => $this->load->view('backend/locations/create_countries',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	// -->

	public function create_cities($country_id)
	{
	
		if($this->input->post())
		{
			$data = array(
				'name' => $this->input->post('name'),
				'country_id' => $country_id
			);
			$this->Location_model->create_city($data);
			redirect('/backend/locations/cities/'.$country_id);
		}
	
		$country = $this->Location_model->country($country_id);

		$content_data = array(
			'country' => $country
		);
		
		$data = array(
			'title' => 'South4Rent - Create city',
			'content' => $this->load->view('backend/locations/create_cities',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
		
	// -->

	public function create_neighborhoods($city_id)
	{
	
		if($this->input->post())
		{
			$data = array(
				'name' => $this->input->post('name'),
				'city_id' => $city_id
			);
			$this->Location_model->create_neighborhood($data);
			redirect('/backend/locations/neighborhoods/'.$city_id);
		}
	
		$city = $this->Location_model->city($city_id);

		$content_data = array(
			'city' => $city
		);
		
		$data = array(
			'title' => 'South4Rent - Create Neighborhood',
			'content' => $this->load->view('backend/locations/create_neighborhoods',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function delete_countries($id)
	{
		$this->Location_model->delete($id,'countries');
		redirect($this->input->server('HTTP_REFERER'));	
	}
	
	public function delete_cities($id)
	{
		$this->Location_model->delete($id,'cities');
		redirect($this->input->server('HTTP_REFERER'));	
	}
	
	public function delete_neighborhoods($id)
	{
		$this->Location_model->delete($id,'neighborhoods');
		redirect($this->input->server('HTTP_REFERER'));	
	}
	
}

/* End of file locations.php */
/* Location: ./application/controllers/backend/locations.php */