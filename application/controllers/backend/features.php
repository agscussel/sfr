<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Features extends MY_Backend {

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Feature_model');
		
		$this->require_login();
	
	}

	public function index()
	{
		
		$content_data = array(
			'features' => $this->Feature_model->all()
		);
		
		$data = array(
			'title' => 'South4Rent - Features list',
			'content' => $this->load->view('backend/features/list',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function edit($id)
	{
	
		if($this->input->post())
		{
			$data = array(
				'name_es' => $this->input->post('name_es'),
				'name_en' => $this->input->post('name_en'),
				'name_pt' => $this->input->post('name_pt')
			);
			
			$update = $this->Feature_model->update($id, $data);
			
			redirect('/backend/features');		
			
		}
		
		$content_data = array(
			'feature' => $this->Feature_model->fetchById($id)
		);
		
		$data = array(
			'title' => 'South4Rent - Features edit',
			'content' => $this->load->view('backend/features/edit',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function create()
	{
	
		if($this->input->post())
		{
			$data = array(
				'name_es' => $this->input->post('name_es'),
				'name_en' => $this->input->post('name_en'),
				'name_pt' => $this->input->post('name_pt')
			);
			
			$create = $this->Feature_model->create($data);
			redirect('/backend/features');

		}
		else
		{
		
			$data = array(
				'title' => 'South4Rent - Create feature',
				'content' => $this->load->view('backend/features/create',NULL,TRUE),
				'account' => $this->session->userdata('account'),
				'segment' => $this->segment

			);
			$this->load->view('backend/layout',$data);
		}
	}
	
	public function delete($id)
	{
		
		$this->Feature_model->delete($id);
		redirect('/backend/features');
	}
	
}

/* End of file features.php */
/* Location: ./application/controllers/backend/features.php */