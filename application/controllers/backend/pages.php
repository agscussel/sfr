<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Backend {

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Page_model');
		
		$this->require_login();
	
	}

	public function index()
	{
		
		$content_data = array(
			'pages' => $this->Page_model->all()
		);
		
		$data = array(
			'title' => 'South4Rent - Pages list',
			'content' => $this->load->view('backend/pages/list',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function edit($id)
	{
	
		if($this->input->post())
		{
			$data = array(
				'title_es' => $this->input->post('title_es'),
				'title_en' => $this->input->post('title_en'),
				'title_pt' => $this->input->post('title_pt'),
				'content_es' => $this->input->post('content_es'),
				'content_en' => $this->input->post('content_en'),
				'content_pt' => $this->input->post('content_pt'),
				'modified' => date('Y-m-d H:i:s',now())
			);
			
			$update = $this->Page_model->update($id, $data);
			
			redirect('/backend/pages');		
			
		}
		
		$content_data = array(
			'page' => $this->Page_model->fetchById($id)
		);
		
		$data = array(
			'title' => 'South4Rent - Pages edit',
			'content' => $this->load->view('backend/pages/edit',$content_data,TRUE),
			'account' => $this->session->userdata('account'),
			'segment' => $this->segment
		);

		$this->load->view('backend/layout',$data);
	}
	
	public function create()
	{
	
		if($this->input->post())
		{
			$data = array(
				'title_es' => $this->input->post('title_es'),
				'title_en' => $this->input->post('title_en'),
				'title_pt' => $this->input->post('title_pt'),
				'content_es' => $this->input->post('content_es'),
				'content_en' => $this->input->post('content_en'),
				'content_pt' => $this->input->post('content_pt'),
				'created' => date('Y-m-d H:i:s',now()),
				'modified' => date('Y-m-d H:i:s',now())
			);
			
			$create = $this->Page_model->create($data);
			redirect('/backend/pages');

		}
		else
		{
		
			$data = array(
				'title' => 'South4Rent - Create page',
				'content' => $this->load->view('backend/pages/create',NULL,TRUE),
				'account' => $this->session->userdata('account'),
				'segment' => $this->segment

			);
			$this->load->view('backend/layout',$data);
		}
	}
	
	public function delete($id)
	{
		
		$this->Page_model->delete($id);
		redirect('/backend/pages');
	}
	
}

/* End of file pages.php */
/* Location: ./application/controllers/backend/pages.php */