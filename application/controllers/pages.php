<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Frontend {

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Page_model');
	}

	
	public function index($id)
	{

		$page = $this->Page_model->translatedPageById($id);
				
		$page_data = array(
			'title' => $page->title,
			'content' => nl2br($page->content)
		);
		
		$data = array(
			'title' => $page->title,
			'content' => $this->load->view('frontend/page',$page_data,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}
}

/* End of file pages.php */
/* Location: ./application/controllers/pages.php */