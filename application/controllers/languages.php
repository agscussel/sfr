<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Languages extends MY_Frontend {

	function __construct()
	{
		
		parent::__construct();	
	
	}

	public function set($lang)
	{
		$this->session->set_userdata('language', $lang);
		redirect($this->input->server('HTTP_REFERER', TRUE));
	}

}

/* End of file languages.php */
/* Location: ./application/controllers/languages.php */