<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends MY_Frontend {

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Location_model');
	
	}

	public function countries()
	{
	
		$this->output->set_content_type('application/json')->set_output(json_encode($this->Location_model->countries()));
	}
	
	public function cities($country_id)
	{
	
		$this->output->set_content_type('application/json')->set_output(json_encode($this->Location_model->cities($country_id)));
	}
	
	public function neighborhoods($city_id)
	{
	
		$this->output->set_content_type('application/json')->set_output(json_encode($this->Location_model->neighborhoods($city_id)));
	}
	

}

/* End of file locations.php */
/* Location: ./application/controllers/locations.php */