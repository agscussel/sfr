<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Frontend {

	function __construct()
	{	
		parent::__construct();

		$this->load->library('email');
		$this->load->model('Page_model');
		$this->load->model('Property_model');
		$this->load->model('Location_model');
	}

	public function index()
	{
		$this->homepage();
	}
	
	public function homepage()
	{
		$this->lang->load('homepage', $this->session->userdata('language'));

		$data = array(
			'title' => lang('homepage_title'),
			'content' => $this->load->view('frontend/homepage', '', TRUE),
			'header' => $this->load->view('frontend/header', '', TRUE),
			'footer' => $this->load->view('frontend/footer', '', TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}

	public function chile_homepage() {
		$this->lang->load('homepage', $this->session->userdata('language'));
		$this->country_selection('chile');

		$content_data = $this->country_landing_data_load($this->session->userdata('cityId_selected'), $this->session->userdata('countryId_selected'));

		$data = array(
			'title' => lang('homepage_title'),
			'content' => $this->load->view('frontend/santiago-chile', $content_data, TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}

	public function brasil_homepage() {
		$this->lang->load('homepage', $this->session->userdata('language'));
		$this->country_selection('brasil');

		$content_data = $this->country_landing_data_load($this->session->userdata('cityId_selected'), $this->session->userdata('countryId_selected'));

		$data = array(
			'title' => lang('homepage_title'),
			'content' => $this->load->view('frontend/sanpablo-brasil', $content_data, TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}

	private function country_selection($countryName) {
		$country_row = $this->Location_model->country_byName($countryName);
		
		if (!$country_row) {
			$country_row = $this->Location_model->defaultCountry();
		} 

		$city_row = $this->Location_model->defaultCity_byCountryId($country_row->id);
		
		$this->session->set_userdata('countryId_selected', $country_row->id);
		$this->session->set_userdata('cityId_selected', $city_row->id);	
	}

	private function country_landing_data_load($cityId, $countryId) {
		$where_options = array(
              '0' => lang('homepage_search_neighborhood')
		);
		
		$neighborhoods = $this->Location_model->neighborhoods_homepage($cityId);

		foreach($neighborhoods as $neighborhood)
		{
			$where_options[$neighborhood->id] = "{$neighborhood->name} ({$neighborhood->properties})";
		}
		
		$rooms_options = array(
              '0' => lang('homepage_search_rooms_plural'),
              '1' => '1',
              '2' => '2',
              '3' => '3',
              '4' => '4',
              '5' => '5'
		);
		
		$content_data = array(
			'featured' => $this->Property_model->findBy(FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, $cityId, $countryId),
			'where_dropdown' => form_dropdown('where', $where_options, NULL, 'id="search_where"'),
			'rooms_dropdown' => form_dropdown('rooms', $rooms_options, NULL, 'id="search_rooms"'),
			'in_input' => form_input('in', NULL, 'id="search_in" placeholder="Check In"'),
			'out_input' => form_input('out', NULL, 'id="search_out" placeholder="Check Out"')
		);

		return $content_data;
	}
	
	public function cityguide()
	{
	
		$this->lang->load('homepage', $this->session->userdata('language'));
		
		$data = array(
			'title' => lang('homepage_title'),
			'content' => $this->load->view('frontend/cityguide',NULL,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}
	
	public function contact()
	{
	
		$this->lang->load('contact', $this->session->userdata('language'));
	
		$confirmation = FALSE;
	
		if($this->input->post())
		{
			$message = "
				<div style='padding:20px; background-color:#A1ECFF; color:#333'>
					First and Last name: ".$this->input->post('first_last')."<br>
					Email: ".$this->input->post('email')."<br>
					Telephone: ".$this->input->post('telephone')."<br>
					Comments: ".$this->input->post('comments')."
				</div>
			";
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from($this->input->post('email'), $this->input->post('first_last'));
			$this->email->to('info@south4rent.com');
			$this->email->subject("{".mailSubjectPrefix($this->session->userdata('countryId_selected'))." - South4Rent} Contact form");
			$this->email->message($message);

			$this->email->send();
			
			$confirmation = TRUE;
		}
		
		$content_data = array(
			'confirmation' => $confirmation,
			'information' => $this->Page_model->translatedPageById(9)->content,
		);
		
		$data = array(
			'title' => lang('contact_title'),
			'content' => $this->load->view('frontend/contact',$content_data,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}
	
	public function tell_a_friend()
	{
	
		$this->lang->load('tell-a-friend', $this->session->userdata('language'));
	
		$confirmation = FALSE;
	
		if($this->input->post())
		{
			$message = "
				<div style='padding:20px; background-color:#A1ECFF; color:#333'>
					First and Last name: ".$this->input->post('first_last')."<br>
					Email: ".$this->input->post('email')."<br>
					Telephone: ".$this->input->post('telephone')."<br>
					Comments: ".$this->input->post('comments')."
				</div>
			";
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from($this->input->post('email'), $this->input->post('first_last'));
			$this->email->to('info@south4rent.com');
			$this->email->subject("{".mailSubjectPrefix($this->session->userdata('countryId_selected'))." - South4Rent} tell a friend");
			$this->email->message($message);

			$this->email->send();
			
			$confirmation = TRUE;
		}
		
		$content_data = array(
			'confirmation' => $confirmation,
			'information' => $this->Page_model->translatedPageById(9)->content,
		);
		
		$data = array(
			'title' => lang('tell_a_friend_title'),
			'content' => $this->load->view('frontend/tell-a-friend',$content_data,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */