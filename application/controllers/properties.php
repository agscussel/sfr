<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Properties extends MY_Frontend {

	private $countryId_selected;
	private $cityId_selected;

	function __construct()
	{
		
		parent::__construct();	
		$this->load->model('Property_model');
		$this->load->model('Photo_model');
		$this->load->model('Page_model');
		$this->load->model('Availability_model');
		$this->load->model('Date_model');
		$this->load->model('Feature_model');
		$this->load->model('Location_model');
		$this->load->library('email');
		$this->load->library('pagination');
		
		$this->lang->load('properties', $this->session->userdata('language'));

		$this->countryId_selected = $this->session->userdata('countryId_selected');
		if (!$this->countryId_selected) {
			// Retrieve default value
			$this->countryId_selected = $this->Location_model->defaultCountry()->id;
		}
		
		$this->cityId_selected = $this->session->userdata('cityId_selected');
		if (!$this->cityId_selected) {
			// Retrieve default value
			$this->cityId_selected = $this->Location_model->defaultCity_byCountryId($this->countryId_selected)->id;
		}

	}

	
	public function create()
	{
		
		$confirmation = FALSE;
	
		if($this->input->post())
		{
			$message = "
				<div style='padding:20px; background-color:#A1ECFF; color:#333'>
					First and Last name: ".$this->input->post('first_last')."<br>
					Email: ".$this->input->post('email')."<br>
					Telephone: ".$this->input->post('telephone')."<br>
				</div>
			";
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from($this->input->post('email'), $this->input->post('first_last'));
			$this->email->to('info@south4rent.com');
			$this->email->subject("{".mailSubjectPrefix($this->session->userdata('countryId_selected'))." - South4Rent} list your property form");
			$this->email->message($message);

			$this->email->send();
			
			$confirmation = TRUE;
		}
		
		$content_data = array(
			'confirmation' => $confirmation,
			'information' => $this->Page_model->translatedPageById(8)->content
		);
		
		$data = array(
			'title' => lang('properties_title'),
			'content' => $this->load->view('frontend/properties/create',$content_data,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}

	public function search($where = 0, $check_in = 0, $check_out = 0, $rooms = 0, $per_page = 1)
	{
		
		$this->lang->load('homepage', $this->session->userdata('language'));
		
		$per_page = $this->input->get('per_page');
		$check_in = $this->input->get('i');
		$check_out = $this->input->get('o');
		$where = $this->input->get('w');
		$rooms = $this->input->get('r');
		$sort = $this->input->get('s');
		
		if($check_in == 0)
		{
			$check_in = '';
			$startDate = NULL;
		}
		else
		{
			$startDate = gmdate("Y-m-d", strtotime($check_in));
		}
		
		if($check_out == 0)
		{
			$check_out = '';
			$endDate = NULL;
		}
		else
		{
			$endDate = gmdate("Y-m-d", strtotime($check_out));
		}

		
		$where_options = array(
              '0' => lang('homepage_search_neighborhood')
		);
		$neighborhoods = $this->Location_model->neighborhoods_homepage($this->cityId_selected);
		foreach($neighborhoods as $neighborhood)
		{
			$where_options[$neighborhood->id] = "{$neighborhood->name} ({$neighborhood->properties})";
		}
		
		$rooms_options = array(
              '0' => lang('homepage_search_rooms_plural'),
              '1' => '1',
              '2' => '2',
              '3' => '3',
              '4' => '4',
              '5' => '5'
		);
		
		$config['base_url'] = site_url()."properties/search/?w={$where}&i={$check_in}&o={$check_out}&r={$rooms}&s={$sort}";		
		$config['total_rows'] = $this->Property_model->findByTotal($startDate, $endDate, NULL, $where, $rooms, $this->cityId_selected, $this->countryId_selected);		
		$config['per_page'] = 10; 
		$config['page_query_string'] = TRUE;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['num_links'] = 20;
		$this->pagination->initialize($config); 
		
		$content_data = array(
			'properties' => $this->Property_model->findBy($config['per_page'], $per_page, $startDate, $endDate, NULL, $where, $rooms, $sort, $this->cityId_selected, NULL),
			'where_dropdown' => form_dropdown('where', $where_options, $where, 'id="search_where"'),
			'rooms_dropdown' => form_dropdown('rooms', $rooms_options, $rooms, 'id="search_rooms"'),
			'in_input' => form_input('in', $check_in, 'id="search_in" placeholder="Check In"'),
			'out_input' => form_input('out', $check_out, 'id="search_out" placeholder="Check Out"'),
			'sort' => $sort,
			'link' => site_url()."properties/search/?w={$where}&i={$check_in}&o={$check_out}&r={$rooms}&s=",
			'pagination' => $this->pagination->create_links()
		);

		
		$data = array(
			'title' => lang('properties_title'),
			'content' => $this->load->view('frontend/properties/search',$content_data,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->parser->parse('frontend/layout', $data);
		
		
	}
	
	function currency($from, $to)
	{

		$url = "hl=en&q=1$from=?$to";
		$rawdata = file_get_contents("http://google.com/ig/calculator?".$url);
		$data = explode('"', $rawdata);
		$data = explode(' ', $data['3']);
		$var = $data['0'];
		//$var = @number_format($var,2);
		return $var;
	}

	public function contact($id)
	{
	
		$confirmation = FALSE;		
		$property = $this->Property_model->findById($id);
	
		if($this->input->post())
		{
			$message = "
				<div style='padding:20px; background-color:#A1ECFF; color:#333'>
					First and Last name: ".$this->input->post('first_last')."<br>
					Email: ".$this->input->post('email')."<br>
					Telephone: ".$this->input->post('telephone')."<br>
					Comments: ".$this->input->post('comments')."<br>
					Dates: ".$this->input->post('check_in')." - ".$this->input->post('check_out')."<br>
					How did you find about South4Rent: ".$this->input->post('how')."<br>
					Guests: ".$this->input->post('guests')."<br>
					Children: ".$this->input->post('children')."<br>
					Property: http://www.south4rent.com/properties/view/". $property->id ."
				</div>
			";
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from($this->input->post('email'), $this->input->post('first_last'));
			$this->email->to('info@south4rent.com');
			$this->email->subject("{".mailSubjectPrefix($this->session->userdata('countryId_selected'))." - South4Rent} Property information - Contact form");
			$this->email->message($message);

			$this->email->send();
			
			$confirmation = TRUE;
		}
		
		
		$content_data = array(
			'confirmation' => $confirmation,
			'information' => $this->Page_model->translatedPageById(11)->content,
			'property' => $property,
		);
		
		$data = array(
			'title' => lang('properties_contact_title'),
			'content' => $this->load->view('frontend/properties/contact',$content_data,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
	}
	

	
	public function view($id)
	{
		$property = $this->Property_model->findById($id);
		
		if(!$property)
		{
			die('Not Found');
		} 
		
		$photos = $this->Photo_model->findByPropertyId($id);
		foreach($photos as $photo)
		{
			if($photo->default==1)
			{
				$default = $photo->id;
			}
		}
		
		$bad_dates = $this->Date_model->fetchByPropertyId($id);
		
		$availability = $this->Availability_model->fetchByPropertyId($id);
		
		$dates_array = array();
		foreach($availability as $date)
		{
			$days = $this->Availability_model->days($date->from_date, $date->to_date);
			foreach($days as $day)
			{
				$day_exploded = explode('-', $day);
				$year = $day_exploded[0];
				$month = str_pad($day_exploded[1], 2, "0", STR_PAD_LEFT);
				$day = str_pad($day_exploded[2], 2, "0", STR_PAD_LEFT);
				$compiled_date = array($year,$month,$day,$date->status);
				$dates_array[] = $compiled_date;
			}
		}
		
		//print_r($dates_array);
		
		$content_data = array(
			'property' => $property,
			'photos' => $photos,
			'features' => $this->Feature_model->propertyFeatures($id),
			//'location' => $property->address_street.' '.$property->address_number.', '.$property->city_name.', '.$property->neigborhood_name.', '.$property->country_name,
			'default' => $default,
			// 'daily_price_usd' => number_format($this->currency('BRL','USD')*$property->daily_price, 2, '.', ''),
			// 'weekly_price_usd' => number_format($this->currency('BRL','USD')*$property->weekly_price, 2, '.', ''),
			// 'monthly_price_usd' => number_format($this->currency('BRL','USD')*$property->monthly_price, 2, '.', ''),
			'currency_simbol' => currencySimbol($property->country_id),
			'bad_dates' => $bad_dates,
			'availability' => $dates_array
		);
		
		$data = array(
			'title' => $property->name .' - '.$property->title,
			'content' => $this->load->view('frontend/properties/view',$content_data,TRUE),
			'header' => $this->load->view('frontend/header', array('citySelectedId' => $this->session->userdata('cityId_selected')), TRUE),
			'footer' => $this->load->view('frontend/footer', array('countrySelectedId' => $this->session->userdata('countryId_selected')), TRUE)
		);
		
		$this->load->view('frontend/layout', $data);
		
		if($this->input->ip_address()=='190.195.101.214')
		{
			//print_r($content_data);
		}
	}
}

/* End of file properties.php */
/* Location: ./application/controllers/properties.php */