<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Backend extends CI_Controller
{

	var $segment;
	
	function __construct()
	{
		
		parent::__construct();
		
		$this->segment = $this->uri->segment(2);
		
	}
	
	function require_login($redirect = TRUE)
	{
		$is_logged_in = $this->session->userdata('account') ? TRUE : FALSE;
		
		if($redirect == TRUE && $is_logged_in == FALSE)
		{
			$this->session->set_flashdata('error', 'Session expired');
			$this->session->set_flashdata('redir_to', current_url());
						
			redirect('/backend/accounts/signin?session_expired');
		}
		
		return $is_logged_in;
	}
	
}