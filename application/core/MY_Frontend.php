<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Frontend extends CI_Controller
{
	function __construct()
	{
		
		parent::__construct();
		
		$this->load->library('parser');
		
		if(!$this->session->userdata('language'))
		{
			$this->session->set_userdata('language', 'spanish');
		}

		$language = $this->session->userdata('language');
		
		$this->lang->load('global', $language);
		$this->lang->load('header', $language);
		$this->lang->load('footer', $language);
		
	}
	
}