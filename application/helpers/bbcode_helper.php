<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('bbcode'))
{
	function bbcode($str = '')
	{
	
		$find = array(
			"'\[h1\](.*?)\[/h1\]'is",
			"'\[h2\](.*?)\[/h2\]'is",
			"'\[strong\](.*?)\[/strong\]'is",
			"'\[b\](.*?)\[/b\]'is",
			"'\[i\](.*?)\[/i\]'is",
			"'\[u\](.*?)\[/u\]'is",
			"'\[s\](.*?)\[/s\]'is",
			"'\[img\](.*?)\[/img\]'i",
			"'\[url\](.*?)\[/url\]'i",
			"'\[url=(.*?)\](.*?)\[/url\]'i",
			"'\[link\](.*?)\[/link\]'i",
			"'\[link=(.*?)\](.*?)\[/link\]'i"
		);
	
		$replace = array(
			'<h1>\\1</h1>',
			'<h2>\\1</h2>',
			'<strong>\\1</strong>',
			'<strong>\\1</strong>',
			'<em>\\1</em>',
			'<u>\\1</u>',
			'<s>\\1</s>',
			'<img src="\\1" alt="" align="absmiddle" />',
			'<a href="\\1">\\1</a>',
			'<a href="\\1">\\2</a>',
			'<a href="\\1">\\1</a>',
			'<a href="\\1">\\2</a>'
		);
	
		return preg_replace($find, $replace, $str);
	}
} 