<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('currencySimbol'))
{
	function currencySimbol($country_id = '')
	{
		$currency_simbol = "";
		switch($country_id) {
			case '1':
				$currency_simbol = "R$";
				break;
			case '4':
				$currency_simbol = "CLP$";
				break;

		}
		return $currency_simbol;
	}
} 