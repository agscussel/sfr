<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('mailSubjectPrefix'))
{
	function mailSubjectPrefix($country_id)
	{
		$mail_subject_prefix = "";
		switch($country_id) {
			case '1':
				$mail_subject_prefix = "BR";
				break;
			case '4':
				$mail_subject_prefix = "CL";
				break;

		}
		return $mail_subject_prefix;
	}
} 