<?php
class Account_model extends CI_Model {

	function create($array)
	{

		$this->db->insert('accounts',$array);			
		return TRUE;	

	}

	function authenticate($email,$password)
	{
		
		$this->db->select('id, email, name');
		$this->db->from('accounts');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$this->db->where('deleted',0);

		$query = $this->db->get();

		if($query->num_rows() === 1)
		{
			return $row = $query->row();
		}
		else
		{
			return false;
		}
	}
	
	function all()
	{
		
		$this->db->select('*');
		$this->db->from('accounts');
		$this->db->where('deleted',0);
		
		return $this->db->get()->result();
	
	}
	
	function update($id, $array)
	{
		$this->db->where('id', $id);
		$this->db->update('users', $array); 
		
		return TRUE;
	}
	
	
	function delete($id = NULL)
	{
		$data = array(
			'deleted' => 1
		);
		
		$this->db->where('id',$id);
		$this->db->update('accounts', $data); 
	
	}
	
}