<?php
class Feature_model extends CI_Model {
	
	function create($array)
	{

		$this->db->insert('features',$array);			
		return TRUE;	
	}
	
	function all()
	{
		
		$this->db->select('*');
		$this->db->from('features');
		$this->db->where('deleted',0);
		
		return $this->db->get()->result();
	}
	
	function fetchById($id = NULL)
	{
		
		$this->db->select('*');
		$this->db->from('features');
		$this->db->where('deleted',0);
		$this->db->where('id',$id);
		
		return $this->db->get()->row();
	}

	function delete($id = NULL)
	{

		$this->db->where('id', $id);
		$this->db->delete('features');
	}

	function update($id, $array)
	{
		
		$this->db->where('id',$id);
		$this->db->update('features', $array); 
	}
	
	function clearPropertyFeatures($id = NULL)
	{
		
		$this->db->delete('properties_features', array('property_id' => $id)); 
	}
	
	// -->
	
	function assignFeature($array)
	{

		$this->db->insert('properties_features',$array);			
		return TRUE;	
	}
	
	function propertyFeatures($property_id)
	{
		switch($this->session->userdata('language'))
		{
			case 'english':
				$this->db->select('features.name_en name');
			break;
			
			case 'portuguese':
				$this->db->select('features.name_pt name');
			break;
			
			case 'spanish':
				$this->db->select('features.name_es name');
			break;
		}
		$this->db->select('features.id');
        $this->db->from('features');
        $this->db->join('properties_features', 'properties_features.feature_id = features.id AND properties_features.property_id = '.$property_id);
        return $this->db->get()->result();
	}
	
	function checkedFeatures($property_id)
	{

        $this->db->select('features.id, features.name_en, IF(properties_features.id,1,0) checked',FALSE);
        $this->db->from('features');
        $this->db->join('properties_features', 'properties_features.feature_id = features.id AND properties_features.property_id ='.$property_id,'left');
        $this->db->group_by('features.id');
        return $this->db->get()->result();
	}
}