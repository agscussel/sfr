<?php
class Location_model extends CI_Model {

	function delete($id, $table)
	{
		
		$this->db->where('id', $id);
		$this->db->delete($table); 
		return TRUE;	
	}

	function countries()
	{
		
		$this->db->select('id, name');
		$this->db->from('countries');
		
		return $this->db->get()->result();	
	}

	function defaultCity_byCountryId($country_id) {
		$this->db->select('id, name');
		$this->db->from('cities');
		$this->db->where('country_id',$country_id);
		return $this->db->get()->row();
	}

	function defaultCountry() {
		$this->db->select('id, name');
		$this->db->from('countries');
		return $this->db->get()->row();
	}
	
	function cities($country_id)
	{
		
		$this->db->select('id, name');
		$this->db->from('cities');
		$this->db->where('country_id',$country_id);
		$this->db->order_by('name','asc');
		
		return $this->db->get()->result();
	
	}
	
	function neighborhoods($city_id)
	{
		
		$this->db->select('id, name');
		$this->db->from('neighborhoods');
		$this->db->where('city_id',$city_id);
		$this->db->order_by('name','asc');
		
		return $this->db->get()->result();
	
	}
	
	function neighborhoods_homepage($city_id)
	{
		
		$this->db->select('neighborhoods.id, neighborhoods.name, COUNT(properties.id) properties', FALSE);
		$this->db->from('neighborhoods');
		$this->db->join('properties','properties.neighborhood_id = neighborhoods.id AND properties.visible = 1 AND properties.deleted = 0');
		$this->db->where('neighborhoods.city_id',$city_id);
		$this->db->group_by('neighborhoods.id');
		$this->db->order_by('neighborhoods.name','asc');
		return $this->db->get()->result();
	
	}
	
	// -->
	
	function country($id)
	{
		
		$this->db->select('id, name');
		$this->db->from('countries');
		$this->db->where('id',$id);
		
		return $this->db->get()->row();	
	}

	function country_byName($countryName) {
		$this->db->select('id, name');
		$this->db->from('countries');
		$this->db->where('lower(name)', $countryName);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}	
	}

	function country_byCityId($cityId)
	{
		$this->db->select('country_id');
		$this->db->from('cities');
		$this->db->where('id',$cityId);
		
		$cityRow = $this->db->get()->row(); 

		$countryRow;

		if ($cityRow) 
		{
			$countryId = $cityRow->country_id;

			$this->db->select('id, name');
			$this->db->from('countries');
			$this->db->where('id',$countryId);

			$countryRow = $this->db->get()->row();
		}

		return $countryRow;	
	}
	
	function city($id)
	{
		
		$this->db->select('id, name');
		$this->db->from('cities');
		$this->db->where('id',$id);
		
		return $this->db->get()->row();	
	}
	
	function neightborhood($id)
	{
		
		$this->db->select('id, name');
		$this->db->from('neighborhoods');
		$this->db->where('id',$id);
		
		return $this->db->get()->row();	
	}
	
	// -->
	
	function create_country($array)
	{

		$this->db->insert('countries',$array);			
		return TRUE;	
	}
	
	function create_city($array)
	{

		$this->db->insert('cities',$array);			
		return TRUE;	
	}
	
	function create_neighborhood($array)
	{

		$this->db->insert('neighborhoods',$array);			
		return TRUE;	
	}
	
}