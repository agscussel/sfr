<?php
class Property_model extends CI_Model {
	
	function create($array)
	{

		$this->db->insert('properties',$array);			
		$property_id = $this->db->insert_id();
			
		return $property_id;	

	}
	
	function all()
	{
		
		$this->db->select('*');
		$this->db->from('properties');
		$this->db->where('deleted',0);
		
		return $this->db->get()->result();
	
	}

	function propertyFromCountryId($country_id, $property_id)
	{
		
		$this->db->select('*');
		$this->db->from('properties');
		$this->db->where('id',$property_id);
		$this->db->where('country_id',$country_id);
		return $this->db->count_all_results() > 0;

	}

	function findByTotal($check_in = FALSE, $check_out = FALSE, $featured = FALSE , $neighborhood_id = FALSE, $rooms = FALSE, $city_id = FALSE, $country_id = FALSE)
	{
		
		$this->db->select('properties.id');
		$this->db->from('properties');
		$this->db->join('countries', 'countries.id = properties.country_id');
		$this->db->join('cities', 'cities.id = properties.city_id');
		$this->db->join('photos', 'photos.property_id = properties.id AND photos.default = 1 AND photos.deleted = 0');
		$this->db->join('neighborhoods', 'neighborhoods.id = properties.neighborhood_id');
		if($check_in && $check_out)
		{
			$where_sql = "
				properties.id NOT IN (
					SELECT availability.property_id
					FROM availability
					WHERE (from_date >= '$check_in' AND from_date >= '$check_out') OR (from_date <= '$check_in' AND to_date >= '$check_out') OR (from_date <= '$check_in' AND to_date <= '$check_out') 
				)
			";
			$this->db->where($where_sql, NULL, FALSE);
		}
		
		if($city_id)
		{
			$this->db->where('properties.city_id',$city_id);		
		}
		if($country_id)
		{
			$this->db->where('properties.country_id',$country_id);		
		}
		if($neighborhood_id)
		{
			$this->db->where('properties.neighborhood_id',$neighborhood_id);		
		}
		if($rooms)
		{
			$this->db->where('properties.rooms',$rooms);		
		}
		if($featured)
		{
			$this->db->where('properties.featured',1);		
		}
		$this->db->where('properties.deleted',0);
		$this->db->where('properties.visible',1);
		
		return $this->db->count_all_results();
	
	}

	function findBy($limit = FALSE, $start = FALSE, $check_in = FALSE, $check_out = FALSE, $featured = FALSE , $neighborhood_id = FALSE, $rooms = FALSE, $sort = FALSE, $city_id = FALSE, $country_id = FALSE)
	{
		
		$this->db->select('properties.*, countries.name country_name, cities.name city_name, neighborhoods.name neigborhood_name, photos.id photo');
		$this->db->from('properties');
		$this->db->join('countries', 'countries.id = properties.country_id');
		$this->db->join('cities', 'cities.id = properties.city_id');
		$this->db->join('photos', 'photos.property_id = properties.id AND photos.default = 1 AND photos.deleted = 0');
		$this->db->join('neighborhoods', 'neighborhoods.id = properties.neighborhood_id');
		if($check_in && $check_out)
		{
			$where_sql = "
				properties.id NOT IN (
					SELECT availability.property_id
					FROM availability
					WHERE (from_date >= '$check_in' AND from_date >= '$check_out') OR (from_date <= '$check_in' AND to_date >= '$check_out') OR (from_date <= '$check_in' AND to_date <= '$check_out') 
				)
			";
			$this->db->where($where_sql, NULL, FALSE);
		}
		
		if($city_id)
		{
			$this->db->where('properties.city_id',$city_id);		
		}
		if($country_id)
		{
			$this->db->where('properties.country_id',$country_id);		
		}
		if($neighborhood_id)
		{
			$this->db->where('properties.neighborhood_id',$neighborhood_id);		
		}
		if($rooms)
		{
			$this->db->where('properties.rooms',$rooms);		
		}
		if($featured)
		{
			$this->db->where('properties.featured',1);		
		}
		$this->db->where('properties.deleted',0);
		$this->db->where('properties.visible',1);
		
		if($sort)
		{
			switch($sort){
				case 1:
					$this->db->order_by('properties.monthly_price','desc');
					break;
				case 2:
					$this->db->order_by('properties.monthly_price','asc');
					break;
				case 3:
					$this->db->order_by('neighborhoods.name','asc');
					break;
				case 4:
					$this->db->order_by('properties.indoor_space','desc');
					break;
			}	
		}
		else
		{
			$this->db->order_by('id','desc');
		}
		
		if($limit)
		{
			$this->db->limit($limit, $start);
		}	
		
		return $this->db->get()->result();
	
	}
	
	function findByid($id = NULL)
	{
		
		$this->db->select('properties.*, countries.name country_name, cities.name city_name, neighborhoods.name neigborhood_name, photos.id photo');
		switch($this->session->userdata('language'))
		{
			case 'english':
				$this->db->select('properties.title_en title, properties.description_en description');
			break;
			
			case 'portuguese':
				$this->db->select('properties.title_pt title, properties.description_pt description');
			break;
			
			case 'spanish':
				$this->db->select('properties.title_es title, properties.description_es description');
			break;
		}	
		$this->db->from('properties');
		$this->db->join('countries', 'countries.id = properties.country_id');
		$this->db->join('cities', 'cities.id = properties.city_id');
		$this->db->join('photos', 'photos.property_id = properties.id AND photos.default = 1 AND photos.deleted = 0');
		$this->db->join('neighborhoods', 'neighborhoods.id = properties.neighborhood_id');
		//$this->db->where('properties.deleted',0);
		$this->db->where('properties.id',$id);
		
		return $this->db->get()->row();
	
	}
	
	function findByidBackend($id = NULL)
	{
		
		$this->db->select('properties.*, countries.name country_name, cities.name city_name, neighborhoods.name neigborhood_name');
		$this->db->from('properties');
		$this->db->join('countries', 'countries.id = properties.country_id');
		$this->db->join('cities', 'cities.id = properties.city_id');
		$this->db->join('neighborhoods', 'neighborhoods.id = properties.neighborhood_id');
		$this->db->where('properties.deleted',0);
		$this->db->where('properties.id',$id);
				
		return $this->db->get()->row();
	
	}

	function delete($id = NULL)
	{
		$data = array(
			'deleted' => 1
		);
		
		$this->db->where('id',$id);
		$this->db->update('properties', $data); 
	
	}

	function update($id, $array)
	{
		
		$this->db->where('id',$id);
		$this->db->update('properties', $array); 
	
	}
}