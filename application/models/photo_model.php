<?php
class Photo_model extends CI_Model {
	
	function create($array)
	{

		$this->db->insert('photos',$array);			
		$photo_id = $this->db->insert_id();
			
		return $photo_id;	

	}
	
	function findByid($id = NULL)
	{
		
		$this->db->select('*');
		$this->db->from('photos');
		$this->db->where('deleted',0);
		$this->db->where('id',$id);
		
		return $this->db->get()->row();
	
	}
	
	function findByPropertyId($id = NULL)
	{
		
		$this->db->select('*');
		$this->db->from('photos');
		$this->db->where('deleted',0);
		$this->db->where('property_id',$id);
		$this->db->order_by('id','asc');
		
		return $this->db->get()->result();
	
	}
	
	function makeDefault($photo_id = NULL, $property_id)
	{
		
		$this->db->where('property_id',$property_id);
		$this->db->update('photos', array('default' => 0)); 
		
		if($photo_id)
		{
			$this->db->where('id',$photo_id);
			$this->db->update('photos', array('default' => 1));
		}
	
	}

	function delete($id = NULL)
	{
		$data = array(
			'deleted' => 1
		);
		
		$this->db->where('id',$id);
		$this->db->update('photos', $data); 
	
	}

}