<?php
class Availability_model extends CI_Model {
	
	function create($array)
	{

		$this->db->insert('availability',$array);			
		return TRUE;	
	}
	
	function fetchByPropertyId($id = NULL)
	{
		
		$this->db->select('*');
		$this->db->from('availability');
		$this->db->where('property_id',$id);
		$this->db->order_by('from_date','asc');
		
		return $this->db->get()->result();
	}
	
	function delete($id = NULL)
	{
		
		$this->db->delete('availability', array('id' => $id)); 
	}
	
	function days($sStartDate, $sEndDate)
	{
		$sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
		$sEndDate = gmdate("Y-m-d", strtotime($sEndDate));
		$aDays[] = $sStartDate;
		$sCurrentDate = $sStartDate;
		while($sCurrentDate < $sEndDate)
		{
			$sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));
			$aDays[] = $sCurrentDate;
		}

		return $aDays;
	}
}