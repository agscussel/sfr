<?php
class Date_model extends CI_Model {
	
	function create($array)
	{

		$this->db->insert('dates',$array);			
		return TRUE;	
	}
	
	function fetchByPropertyId($id = NULL)
	{
		
		$this->db->select('*');
		$this->db->from('dates');
		$this->db->where('property_id',$id);
		
		return $this->db->get()->result();
	}
	
	function clearDates($id = NULL)
	{
		
		$this->db->delete('dates', array('property_id' => $id)); 
	}
	
}