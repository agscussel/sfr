<?php
class Page_model extends CI_Model {
	
	function create($array)
	{

		$this->db->insert('pages',$array);			
		return TRUE;	

	}
	
	function all()
	{
		
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('deleted',0);
		
		return $this->db->get()->result();
	
	}
	
	function fetchById($id = NULL)
	{
		
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('deleted',0);
		$this->db->where('id',$id);
		
		return $this->db->get()->row();
	
	}
	
	function translatedPageById($id = NULL)
	{
		
		switch($this->session->userdata('language'))
		{
			case 'english':
				$this->db->select('title_en title, content_en content');
			break;
			
			case 'portuguese':
				$this->db->select('title_pt title, content_pt content');
			break;
			
			case 'spanish':
				$this->db->select('title_es title, content_es content');
			break;
		}
		
		$this->db->from('pages');
		$this->db->where('deleted',0);
		$this->db->where('id',$id);
		
		return $this->db->get()->row();
	
	}

	function delete($id = NULL)
	{
		$data = array(
			'deleted' => 1
		);
		
		$this->db->where('id',$id);
		$this->db->update('pages', $data); 
	
	}

	function update($id, $array)
	{
		
		$this->db->where('id',$id);
		$this->db->update('pages', $array); 
	
	}
}