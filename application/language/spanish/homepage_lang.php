<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['homepage_title'] = "Bienvenido";
$lang['homepage_box_why'] = "¿Por qué South4Rent?";
$lang['homepage_box_list'] = "Agrega tu propiedad";
$lang['homepage_box_cityguide'] = "Cityguide";
$lang['homepage_box_contact_us'] = "Contáctanos";
$lang['homepage_find_the_best_place'] = "Encontrá tu lugar";

$lang['homepage_search_neighborhood'] = "Barrio";

$lang['homepage_search_rooms_plural'] = "Habitaciones";
$lang['homepage_search_rooms_singular'] = "Habitacion";

$lang['homepage_news'] = "Noticias";