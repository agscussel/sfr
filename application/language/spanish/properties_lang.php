<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['properties_title'] 							 = "Propiedades";

$lang['properties_create_title'] 					 = "Agrega tu propiedad";

$lang['properties_create_form_first_last'] 			 = "Nombre y apellido";
$lang['properties_create_form_telephone'] 			 = "Teléfono";
$lang['properties_create_form_send'] 				 = "Enviar";
$lang['properties_create_confirmation'] 			 = "El formulario ha sido enviado";

$lang['properties_view_back']						 = "Volver a la búsqueda";

$lang['properties_view_calendar']					 = "Calendario";
$lang['properties_view_photos']						 = "Fotos";
$lang['properties_view_map']						 = "Mapa";
$lang['properties_view_currency']					 = "Moneda";

$lang['properties_view_general_features']			 = "Caracteristicas generales";
$lang['properties_view_name']						 = "Nombre";
$lang['properties_view_destination']				 = "Barrio";
$lang['properties_view_indoor_space']				 = "Espacio interior";
$lang['properties_view_outdoor_space']				 = "Espacio exterior";
$lang['properties_view_bedrooms']					 = "Habitaciones";
$lang['properties_view_bathrooms']					 = "Baños";
$lang['properties_view_minimum_stay']				 = "Estadía minima";
$lang['properties_view_minimum_stay_days']			 = "Dias";
$lang['properties_view_minimum_stay_weeks']			 = "Semanas";
$lang['properties_view_minimum_stay_months']		 = "Meses";
$lang['properties_view_max_guests']					 = "Max. huespedes";
$lang['properties_view_daily']						 = "Diario";
$lang['properties_view_weekly']						 = "Semanal";
$lang['properties_view_monthly']					 = "Mensual";
$lang['properties_view_description']				 = "Descripcion";

$lang['properties_view_available']					 = "Disponible";
$lang['properties_view_hold']						 = "En espera";
$lang['properties_view_unavailable']				 = "No disponible";

$lang['properties_view_reserve_place']				 = "Reserve su lugar";

$lang['properties_search_neighborhood']	 			 = "Barrio";
$lang['properties_search_rooms']					 = "Habitaciones";
$lang['properties_search_bathrooms']				 = "Baños";
$lang['properties_search_daily']					 = "Diario";
$lang['properties_search_weekly']					 = "Semanal";
$lang['properties_search_monthly']					 = "Mensual";

$lang['properties_search_no_results']				 = "No se encontraron propiedades";
$lang['properties_search_view_property']			 = "Ver esta propiedad";

$lang['properties_search_sort_by']					 = "Ordenar por";
$lang['properties_search_sort_by_max_price']		 = "Mayor precio";
$lang['properties_search_sort_by_min_price']		 = "Menor precio";
$lang['properties_search_sort_by_area']				 = "Barrio";
$lang['properties_search_sort_by_space']			 = "mts2";

/* CONTACT */
$lang['properties_contact_title'] = "Interesado en reservar?";

$lang['properties_contact_form_first_last'] = "Nombre y apellido";
$lang['properties_contact_form_telephone'] = "Teléfono";
$lang['properties_contact_select_your_dates'] = "Seleccione sus fechas";
$lang['properties_contact_how_did_you_found'] = "Como encontró a South4Rent";
$lang['properties_contact_how_did_you_found_option1'] = "Amigo";
$lang['properties_contact_how_did_you_found_option2'] = "Otro";
$lang['properties_contact_number_of_guests'] = "Numero de pasajeros";
$lang['properties_contact_children'] = "Viaja con niños?";
$lang['properties_contact_form_comments'] = "Comentarios";
$lang['properties_contact_form_send'] = "Enviar";
$lang['properties_contact_confirmation'] = "Formulario enviado";
$lang['properties_contact_required'] = "Campo requerido";
$lang['properties_contact_required_error'] = "Complete todos los campos requeridos";
$lang['properties_contact_yes'] = "Si";
$lang['properties_contact_no'] = "No";