<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['footer_about_us'] = "Acerca de";
$lang['footer_press'] = "Prensa";
$lang['footer_jobs'] = "Trabaja con nosotros";
$lang['footer_terms_and_conditions'] = "Términos y condiciones";
$lang['footer_privacy_policy'] = "Política de privacidad";
$lang['footer_contact_us'] = "Contáctenos";
$lang['footer_tell_a_friend'] = "Recomendar a un amigo";