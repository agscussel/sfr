<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['contact_title'] = "Contáctenos";

$lang['contact_form_first_last'] = "Nombre y apellido";
$lang['contact_form_telephone'] = "Teléfono";
$lang['contact_form_comments'] = "Comentarios";
$lang['contact_form_send'] = "Enviar";
$lang['contact_confirmation'] = "Formulario enviado";
