<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['header_why'] = "Por qué South4Rent";
$lang['header_how'] = "Cómo funciona";
$lang['header_language'] = "Idioma";
$lang['header_contact'] = "Contacto";
$lang['header_spanish'] = "Español";
$lang['header_english'] = "Inglés";
$lang['header_portuguese'] = "Portugués";
$lang['header_list_your_property'] = "Agrega tu propiedad";