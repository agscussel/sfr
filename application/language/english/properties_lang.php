<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['properties_title'] 							 = "Properties";

$lang['properties_create_title'] 					 = "List your property";

$lang['properties_create_form_first_last'] 			 = "Full name";
$lang['properties_create_form_telephone'] 			 = "Telephone";
$lang['properties_create_form_send'] 				 = "Send";
$lang['properties_create_confirmation'] 			 = "Form successfully sent";

$lang['properties_view_back']						 = "Back to search";

$lang['properties_view_calendar']					 = "Calendar";
$lang['properties_view_photos']						 = "Photos";
$lang['properties_view_map']						 = "Map";
$lang['properties_view_currency']					 = "Currency";

$lang['properties_view_general_features']			 = "General Features";
$lang['properties_view_name']						 = "Name";
$lang['properties_view_destination']				 = "Area";
$lang['properties_view_indoor_space']				 = "Indoor space";
$lang['properties_view_outdoor_space']				 = "Outdoor space";
$lang['properties_view_bedrooms']					 = "Bedrooms";
$lang['properties_view_bathrooms']					 = "Bathrooms";
$lang['properties_view_minimum_stay']				 = "Minimum stay";
$lang['properties_view_minimum_stay_days']			 = "Days";
$lang['properties_view_minimum_stay_weeks']			 = "Weeks";
$lang['properties_view_minimum_stay_months']		 = "Months";
$lang['properties_view_max_guests']					 = "Max guests";
$lang['properties_view_daily']						 = "Daily";
$lang['properties_view_weekly']						 = "Weekly";
$lang['properties_view_monthly']					 = "Monthly";
$lang['properties_view_description']				 = "Description";

$lang['properties_view_available']					 = "Available";
$lang['properties_view_hold']						 = "On hold";
$lang['properties_view_unavailable']				 = "Unavailable";

$lang['properties_view_reserve_place']				 = "Reserve your place";

$lang['properties_search_neighborhood']				 = "Area";
$lang['properties_search_rooms']					 = "Bedrooms";
$lang['properties_search_bathrooms']				 = "Bathrooms";
$lang['properties_search_daily']					 = "Daily";
$lang['properties_search_weekly']					 = "Weekly";
$lang['properties_search_monthly']					 = "Monthly";

$lang['properties_search_no_results']				 = "No properties found";
$lang['properties_search_view_property']			 = "View property";

$lang['properties_search_sort_by']					 = "Sort by";
$lang['properties_search_sort_by_max_price']		 = "Max price";
$lang['properties_search_sort_by_min_price']		 = "Min price";
$lang['properties_search_sort_by_area']				 = "Area";
$lang['properties_search_sort_by_space']			 = "mts2";


/* CONTACT */
$lang['properties_contact_title'] = "Interested in booking";

$lang['properties_contact_form_first_last'] = "Full name";
$lang['properties_contact_form_telephone'] = "Telephone";
$lang['properties_contact_select_your_dates'] = "Select your dates";
$lang['properties_contact_how_did_you_found'] = "How did you find about South4Rent";
$lang['properties_contact_how_did_you_found_option1'] = "Friend";
$lang['properties_contact_how_did_you_found_option2'] = "Other";
$lang['properties_contact_number_of_guests'] = "Number of guests";
$lang['properties_contact_children'] = "Traveling with children?";
$lang['properties_contact_form_comments'] = "Comments";
$lang['properties_contact_form_send'] = "Send";
$lang['properties_contact_confirmation'] = "Form successfully sent";
$lang['properties_contact_required'] = "Required field";
$lang['properties_contact_required_error'] = "Complete all the required fields";
$lang['properties_contact_yes'] = "Yes";
$lang['properties_contact_no'] = "No";
