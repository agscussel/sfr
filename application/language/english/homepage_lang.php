<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['homepage_title'] = "Welcome";
$lang['homepage_box_why'] = "Why South4Rent?";
$lang['homepage_box_list'] = "List your property";
$lang['homepage_box_cityguide'] = "Cityguide";
$lang['homepage_box_contact_us'] = "Contact Us";
$lang['homepage_find_the_best_place'] = "Find your place";

$lang['homepage_search_neighborhood'] = "Area";

$lang['homepage_search_rooms_plural'] = "Bedrooms";
$lang['homepage_search_rooms_singular'] = "Bedroom";

$lang['homepage_news'] = "News";