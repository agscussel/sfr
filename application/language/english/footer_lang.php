<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['footer_about_us'] = "About us";
$lang['footer_press'] = "Press";
$lang['footer_jobs'] = "Work with us";
$lang['footer_terms_and_conditions'] = "Terms and conditions";
$lang['footer_privacy_policy'] = "Privacy policy";
$lang['footer_contact_us'] = "Contact us";
$lang['footer_tell_a_friend'] = "Tell a friend";