<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['header_why'] = "Why South4Rent";
$lang['header_how'] = "How it works";
$lang['header_language'] = "Language";
$lang['header_contact'] = "Contact";
$lang['header_spanish'] = "Spanish";
$lang['header_english'] = "English";
$lang['header_portuguese'] = "Portuguese";
$lang['header_list_your_property'] = "List your property";