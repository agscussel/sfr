<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['contact_title'] = "Contact us";

$lang['contact_form_first_last'] = "Full name";
$lang['contact_form_telephone'] = "Telephone";
$lang['contact_form_comments'] = "Comments";
$lang['contact_form_send'] = "Send";
$lang['contact_confirmation'] = "Form successfully sent";
