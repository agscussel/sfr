<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['footer_about_us'] = "Quem Somos";
$lang['footer_press'] = "Imprensa";
$lang['footer_jobs'] = "Trabalhe conosco";
$lang['footer_terms_and_conditions'] = "Termos e Condições";
$lang['footer_privacy_policy'] = "política de privacidade";
$lang['footer_contact_us'] = "False Conosco";
$lang['footer_tell_a_friend'] = "Encomendar a un amigo";