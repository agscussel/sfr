<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['homepage_title'] = "Bem-vindo";
$lang['homepage_box_why'] = "Por que a South4Rent";
$lang['homepage_box_list'] = "Anuncie seu imóvel";
$lang['homepage_box_cityguide'] = "Guia da Cidade";
$lang['homepage_box_contact_us'] = "Contáctanos";
$lang['homepage_find_the_best_place'] = "Ache seu lugar";

$lang['homepage_search_neighborhood'] = "Bairro";

$lang['homepage_search_rooms_plural'] = "Quartos";
$lang['homepage_search_rooms_singular'] = "Quarto";

$lang['homepage_news'] = "Notícias";