<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['contact_title'] = "Fale conosco";

$lang['contact_form_first_last'] = "Nome e sobrenome";
$lang['contact_form_telephone'] = "Telefone";
$lang['contact_form_comments'] = "Comentarios";
$lang['contact_form_send'] = "Enviar";
$lang['contact_confirmation'] = "Formulário enviado";
