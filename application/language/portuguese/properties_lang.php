<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['properties_title'] 							 = "Imóveis";

$lang['properties_create_title'] 					 = "Anuncie seu imóvel";

$lang['properties_create_form_first_last'] 			 = "Nome e sobrenome";
$lang['properties_create_form_telephone'] 			 = "Telefone";
$lang['properties_create_form_send'] 				 = "Enviar";
$lang['properties_create_confirmation'] 			 = "Formulário enviado";

$lang['properties_view_back']						 = "Voltar à pesquisa";

$lang['properties_view_calendar']					 = "Calendário";
$lang['properties_view_photos']						 = "Fotos";
$lang['properties_view_map']						 = "Mapa";
$lang['properties_view_currency']					 = "Moeda";

$lang['properties_view_general_features']			 = "Características Gerais";
$lang['properties_view_name']						 = "Nome";
$lang['properties_view_destination']				 = "Bairro";
$lang['properties_view_indoor_space']				 = "Espaço interior";
$lang['properties_view_outdoor_space']				 = "Espaço exterior";
$lang['properties_view_bedrooms']					 = "Quartos";
$lang['properties_view_bathrooms']					 = "Banheiros";
$lang['properties_view_minimum_stay']				 = "Estadia mínima";
$lang['properties_view_minimum_stay_days']			 = "Dias";
$lang['properties_view_minimum_stay_weeks']			 = "Semanas";
$lang['properties_view_minimum_stay_months']		 = "Meses";
$lang['properties_view_max_guests']					 = "Máx. hóspedes";
$lang['properties_view_daily']						 = "Diario";
$lang['properties_view_weekly']						 = "Semanal";
$lang['properties_view_monthly']					 = "Mensal";
$lang['properties_view_description']				 = "Descrição";

$lang['properties_view_available']					 = "Disponivel";
$lang['properties_view_hold']						 = "Em espera";
$lang['properties_view_unavailable']				 = "No disponivel";

$lang['properties_view_reserve_place']				 = "Reserve seu lugar";

$lang['properties_search_neighborhood']				 = "Bairro";
$lang['properties_search_rooms']					 = "Quartos";
$lang['properties_search_bathrooms']				 = "Banheiros";
$lang['properties_search_daily']					 = "Diario";
$lang['properties_search_weekly']					 = "Semanal";
$lang['properties_search_monthly']					 = "Mensal";

$lang['properties_search_no_results']				 = "No se encontraron propiedades";
$lang['properties_search_view_property']			 = "Ver este imóvel";

$lang['properties_search_sort_by']					 = "Ordenar por";
$lang['properties_search_sort_by_max_price']		 = "Maior preço";
$lang['properties_search_sort_by_min_price']		 = "Menor preço";
$lang['properties_search_sort_by_area']				 = "Bairro";
$lang['properties_search_sort_by_space']			 = "mts2";

/* CONTACT */
$lang['properties_contact_title'] = "Reserve seu lugar";

$lang['properties_contact_form_first_last'] = "Nome e Sobrenome";
$lang['properties_contact_form_telephone'] = "Telefone";
$lang['properties_contact_select_your_dates'] = "Seleccione sus fechas";
$lang['properties_contact_how_did_you_found'] = "Como você achou a South4Rent";
$lang['properties_contact_how_did_you_found_option1'] = "Amigo";
$lang['properties_contact_how_did_you_found_option2'] = "Outro";
$lang['properties_contact_number_of_guests'] = "número de hóspedes";
$lang['properties_contact_children'] = "Viaja con niños?";
$lang['properties_contact_form_comments'] = "Comentarios";
$lang['properties_contact_form_send'] = "Enviar";
$lang['properties_contact_confirmation'] = "Formulario enviado";
$lang['properties_contact_required'] = "Campo necessário";
$lang['properties_contact_required_error'] = "Complete todos los campos requeridos";
$lang['properties_contact_yes'] = "Si";
$lang['properties_contact_no'] = "No";