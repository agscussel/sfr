<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['header_why'] = "Por que a South4Rent";
$lang['header_how'] = "Como funciona";
$lang['header_contact'] = "Fale Conosco";
$lang['header_list_your_property'] = "Anuncie seu imóvel";