<div class="page-header">
    <h1>Countries <small>list</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li class="active"><a href="/backend/locations/countries"><i class="icon-white icon-th-list"></i> List</a></li>
				<li><a href="/backend/locations/create_countries"><i class="icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<table class="table table-stripped">
			<thead>
				<tr>
					<th># ID</th>
					<th>Title</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($countries as $country):?>
				<tr>
					<td><?php echo $country->id?></td>
					<td><?php echo $country->name?></td>
					<td><a href="/backend/locations/cities/<?php echo $country->id?>" class="btn btn-mini">edit</a> <a href="/backend/locations/delete_countries/<?php echo $country->id?>" class="btn btn-mini btn-danger">delete</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>