<div class="page-header">
    <h1>Cities <small>create</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li><a href="/backend/locations/cities/<?php echo $country->id; ?>"><i class="icon-th-list"></i> List</a></li>
				<li class="active"><a href="/backend/locations/create_cities/<?php echo $country->id; ?>"><i class="icon-white icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab">
				<li><a href="#name">Name</a></li>
			</ul>
 			<?php echo form_open(current_url(),array('class' => 'form-horizontal'))?>
 				<fieldset>
					<div class="tab-content">
						
						<div class="tab-pane" id="name">
						<!-- content -->
							
							<div class="control-group">
								<label class="control-label">name</label>
								<div class="controls">
									<input type="text" name="name" class="span6" placeholder="">
								</div>
							</div>
						<!-- /content -->
						</div>
					</div>
					<div class="form-actions">
						<button class="btn btn-primary" type="submit">Save changes</button>
					</div>
				</fieldset>
			<?php echo form_close()?>
		</div>
	</div>
</div>