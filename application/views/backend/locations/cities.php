<div class="page-header">
    <h1><a href="/backend/locations/countries"><?php echo $country->name; ?></a> > cities list</h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li class="active"><a href="/backend/locations/cities/<?php echo $country->id; ?>"><i class="icon-white icon-th-list"></i> List</a></li>
				<li><a href="/backend/locations/create_cities/<?php echo $country->id; ?>"><i class="icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<table class="table table-stripped">
			<thead>
				<tr>
					<th># ID</th>
					<th>Title</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($cities as $city):?>
				<tr>
					<td><?php echo $city->id?></td>
					<td><?php echo $city->name?></td>
					<td><a href="/backend/locations/neighborhoods/<?php echo $city->id?>" class="btn btn-mini">edit</a> <a href="/backend/locations/delete_cities/<?php echo $city->id?>" class="btn btn-mini btn-danger">delete</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>