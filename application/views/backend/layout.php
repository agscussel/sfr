<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/backend.css" rel="stylesheet">

		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<link rel="shortcut icon" href="/assets/img/favicon.ico">

		<link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/assets/apple-touch-icon-114x114.png">
		
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.0.3/bootstrap.min.js"></script>
		<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-modal.js"></script>
		<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tab.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="/assets/js/backend.js"></script>
	</head>

	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
				
					<ul class="nav pull-right">
						<!--li><a href="/backend/help">Help</a></li-->
						<!--li class="divider-vertical"></li-->
						<li class="dropdown">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $account->name?> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<!--li><a href="/backend/accounts/edit"><i class="icon-user"></i> Edit account</a></li-->
								<li><a href="/backend/accounts/signout"><i class="icon-off"></i> Sign out</a></li>
								<!--li class="divider"></li-->
								<!--li><a href="/backend/help"><i class="icon-question-sign"></i> Help</a></li-->
							</ul>
						</li>
					</ul>

					<ul class="nav">
						<!--li><a href="/backend/welcome">Welcome</a></li-->
						<li <?php if ($segment == 'properties'): ?>class="active"<?php endif; ?> ><a href="/backend/properties">Properties</a></li>
						<li <?php if ($segment == 'pages'): ?>class="active"<?php endif; ?> ><a href="/backend/pages">Pages</a></li>
						<li <?php if ($segment == 'features'): ?>class="active"<?php endif; ?> ><a href="/backend/features">Features</a></li>
						<li <?php if ($segment == 'locations'): ?>class="active"<?php endif; ?> ><a href="/backend/locations">Locations</a></li>
						<!--li class="divider-vertical"></li-->
						<li <?php if ($segment == 'accounts'): ?>class="active"<?php endif; ?> ><a href="/backend/accounts">Accounts</a></li>
					</ul>

				</div>
			</div>
		</div>	

		<div class="container">
			<!-- alert -->
			<div id="alert" class="alert alert-block alert-success hide">
				<a class="close" data-dismiss="alert" href="#">×</a>
				<h4 class="alert-heading">South4Rent</h4>
				South4Rent alert
			</div>
    		<!-- /alert -->
    		
    		<div id="view">
				<?php echo $content?>
			</div>
		</div>
	</body>
</html>
