<div class="page-header">
    <h1>Pages <small>list</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li class="active"><a href="/backend/pages"><i class="icon-white icon-th-list"></i> List</a></li>
				<li><a href="/backend/pages/create"><i class="icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<table class="table table-stripped">
			<thead>
				<tr>
					<th># ID</th>
					<th>Title</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($pages as $page):?>
				<tr>
					<td><?php echo $page->id?></td>
					<td><?php echo $page->title_en?></td>
					<td><a href="/backend/pages/edit/<?php echo $page->id?>" class="btn btn-mini">edit</a> <a href="/backend/pages/delete/<?php echo $page->id?>" class="btn btn-mini btn-danger">delete</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>