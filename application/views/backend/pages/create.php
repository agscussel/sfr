<link type="text/css" rel="stylesheet" href="/assets/js/jwysiwyg/jquery.wysiwyg.css"/>
<script type="text/javascript" src="/assets/js/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="/assets/js/jwysiwyg/controls/wysiwyg.image.js"></script>
<script type="text/javascript" src="/assets/js/jwysiwyg/controls/wysiwyg.link.js"></script>
<script type="text/javascript" src="/assets/js/jwysiwyg/controls/wysiwyg.table.js"></script>
<script type="text/javascript">
/*
(function ($) {
	$(document).ready(function () {
		$('#content_en, #content_es, #content_pt').wysiwyg({
			autoGrow: true,
			//css: '/assets/js/jwysiwyg/custom.css',
			css : { fontFamily: 'Arial, Tahoma', fontSize : '12px', color : '#333333'},
			rmUnusedControls: true,
			controls: {
				bold: { visible : true },
				italic: { visible : true },
				increaseFontSize: { visible : true },
				decreaseFontSize: { visible : true },
				h3: { visible : true },
				h2: { visible : true },
				h1: { visible : true },
				insertHorizontalRule: { visible : true },
				insertUnorderedList: { visible : true },
				insertOrderedList: { visible : true },
				insertImage: { visible : true },
				removeFormat: { visible : true }
			}
		});
	});
})(jQuery);
*/
</script>

<div class="page-header">
    <h1>Pages <small>create</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li><a href="/backend/pages"><i class="icon-th-list"></i> List</a></li>
				<li class="active"><a href="/backend/pages/create"><i class="icon-white icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab">
				<li><a href="#title">Title</a></li>
				<li><a href="#content">Content</a></li>
			</ul>
 			<?php echo form_open(current_url(),array('class' => 'form-horizontal'))?>
 				<fieldset>
					<div class="tab-content">
						
						<div class="tab-pane" id="title">
						<!-- content -->
							
							<div class="control-group">
								<label class="control-label">Title</label>
								<div class="controls">
									<input type="text" name="title_pt" class="span6 margin-bottom" placeholder="Portugues">
									<input type="text" name="title_es" class="span6 margin-bottom" placeholder="Español">
									<input type="text" name="title_en" class="span6" placeholder="English">
								</div>
							</div>
						<!-- /content -->
						</div>

						<div class="tab-pane" id="content">
						<!-- content -->
						
							<div class="control-group">
								<label class="control-label">Content</label>
								<div class="controls">
									<textarea rows="20" cols="80" id="content_pt" name="content_pt" class="span8 margin-bottom" placeholder="Portugues"></textarea>
									<hr>
									<textarea rows="20" cols="80" id="content_es" name="content_es" class="span8 margin-bottom" placeholder="Español"></textarea>
									<hr>
									<textarea rows="20" cols="80" id="content_en" name="content_en" class="span8" placeholder="English"></textarea>
								</div>
							</div>
						
						<!-- /content -->
						</div>

					</div>
					<div class="form-actions">
						<button class="btn btn-primary" type="submit">Save changes</button>
					</div>
				</fieldset>
			<?php echo form_close()?>
		</div>
	</div>
</div>