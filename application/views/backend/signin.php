<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<link href="/assets/css/bootstrap.css" rel="stylesheet">
		<link href="/assets/css/backend.css" rel="stylesheet">

		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<link rel="shortcut icon" href="/assets/img/favicon.ico">

		<link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/assets/apple-touch-icon-114x114.png">
		
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.0.3/bootstrap.min.js"></script>
		<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-modal.js"></script>
		<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tab.js"></script>
		<script type="text/javascript" src="/assets/js/backend.js"></script>
	</head>

	<body>
		<div class="container">    		
    		<!-- content -->
    		<?php echo form_open(current_url(),array('class' => 'form-inline'))?>
				<div class="modal fade" id="signin" style="position: relative; top: auto; left: auto; margin: 0 auto; z-index: 1; max-width: 100%;">
					<div class="modal-header">
						<h3>South4Rent - Sign in</h3>
					</div>
					<div class="modal-body">
						<input name="email" type="text" class="span3" placeholder="Email">
						<input name="password" type="password" class="span3" placeholder="Password">
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
            <?php echo form_close()?>
    		<!-- /content -->
		</div>
	</body>
</html>
