<div class="page-header">
    <h1>Features <small>list</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li class="active"><a href="/backend/features"><i class="icon-white icon-th-list"></i> List</a></li>
				<li><a href="/backend/features/create"><i class="icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<table class="table table-stripped">
			<thead>
				<tr>
					<th># ID</th>
					<th>Icon</th>
					<th>Name</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($features as $feature):?>
				<tr>
					<td><?php echo $feature->id?></td>
					<td><img src="/assets/img/features/<?php echo $feature->id?>.jpg"></td>
					<td><?php echo $feature->name_en?></td>
					<td><a href="/backend/features/edit/<?php echo $feature->id?>" class="btn btn-mini">edit</a> <a href="/backend/features/delete/<?php echo $feature->id?>" class="btn btn-mini btn-danger">delete</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>