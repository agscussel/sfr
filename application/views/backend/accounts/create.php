<div class="page-header">
    <h1>Accounts <small>create</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li><a href="/backend/accounts"><i class="icon-th-list"></i> List</a></li>
				<li class="active"><a href="/backend/accounts/create"><i class="icon-white icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab">
				<li><a href="#details">Details</a></li>
			</ul>
 			<?php echo form_open_multipart(current_url(),array('class' => 'form-horizontal'))?>
 				<fieldset>
					<div class="tab-content">
						
						<div class="tab-pane" id="details">
						<!-- content -->
							
							<div class="control-group">
								<label class="control-label">Account</label>
								<div class="controls">
									<input type="text" name="name" class="span6 margin-bottom" placeholder="Name">
									<input type="text" name="email" class="span6 margin-bottom" placeholder="Email">
									<input type="password" name="password" class="span6" placeholder="Password">
								</div>
							</div>
							
						<!-- /content -->
						</div>
					</div>
					<div class="form-actions">
						<button class="btn btn-primary" type="submit">Save changes</button>
					</div>
				</fieldset>
			<?php echo form_close()?>
		</div>
	</div>
</div>