<div class="page-header">
    <h1>Accounts <small>list</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li class="active"><a href="/backend/accounts"><i class="icon-white icon-th-list"></i> List</a></li>
				<li><a href="/backend/accounts/create"><i class="icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<table class="table table-stripped">
			<thead>
				<tr>
					<th># ID</th>
					<th>Name</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($accounts as $account):?>
				<tr>
					<td><?php echo $account->id?></td>
					<td><?php echo $account->name?></td>
					<td><a href="/backend/accounts/delete/<?php echo $account->id?>" class="btn btn-mini btn-danger">delete</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>