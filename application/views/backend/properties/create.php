<script type="text/javascript">
(function ($) {
	fetchCountries();
})(jQuery);
</script>
<div class="page-header">
    <h1>Properties <small>create</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li><a href="/backend/properties"><i class="icon-th-list"></i> List</a></li>
				<li class="active"><a href="/backend/properties/create"><i class="icon-white icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab">
				<li><a href="#details">Details</a></li>
				<li><a href="#description">Description</a></li>
				<li><a href="#features">Features</a></li>
				<li><a href="#map">Map</a></li>
				<!--li><a href="#calendar">Calendar</a></li-->
				<li><a href="#photos">Photos</a></li>
			</ul>
 			<?php echo form_open_multipart(current_url(),array('class' => 'form-horizontal'))?>
 				<fieldset>
					<div class="tab-content">
						
						<div class="tab-pane" id="details">
						<!-- content -->
							

							<div class="control-group">
								<label class="control-label">Name</label>
								<div class="controls">
									<input type="text" name="name" class="span6">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Title</label>
								<div class="controls">
									<input type="text" name="title_pt" class="span6 margin-bottom" placeholder="Portugues">
									<input type="text" name="title_es" class="span6 margin-bottom" placeholder="Español">
									<input type="text" name="title_en" class="span6" placeholder="English">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Featured</label>
								<div class="controls">
									<label class="radio">
										<input type="radio" value="1" name="featured">
											Yes, Show in Homepage
									</label>
									<label class="radio">
										<input type="radio" checked="checked" value="0" name="featured">
											No
									</label>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label">Status</label>
								<div class="controls">
									<label class="radio">
										<input type="radio" checked="checked" value="1" name="visible">
											Visible
									</label>
									<label class="radio">
										<input type="radio" value="0" name="visible">
											Hidden
									</label>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Location</label>
								<div class="controls">
									<select name="country_id" id="country_id" class="span2">
										<option>Country</option>
									</select>
									<select name="city_id" id="city_id" class="span2">
										<option>City</option>
									</select>
									<select name="neighborhood_id" id="neighborhood_id" class="span2">
										<option>Neighborhood</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Type</label>
								<div class="controls">
									<label class="radio">
										<input type="radio" checked="checked" value="1" name="property_type">
											Apartment
									</label>
									<label class="radio">
										<input type="radio" value="2" name="property_type">
											House
									</label>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Bedrooms</label>
								<div class="controls">
									<select name="rooms" class="span1">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Bathrooms</label>
								<div class="controls">
									<select name="bahtrooms" class="span1">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Max Guests</label>
								<div class="controls">
									<select name="max_guests" class="span1">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
										<option>6</option>
										<option>7</option>
										<option>8</option>
										<option>9</option>
										<option>10</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Price</label>
								<div class="controls">
									<div class="input-prepend margin-bottom">
										<span class="add-on">R$</span><input type="text" placeholder="0.00" name="daily_price" class="span2"> <span class="help-inline">Daily</span>
									</div>
									
									<div class="input-prepend margin-bottom">
										<span class="add-on">R$</span><input type="text" placeholder="0.00" name="weekly_price" class="span2"> <span class="help-inline">Weekly</span>
									</div>
									
									<div class="input-prepend margin-bottom">
										<span class="add-on">R$</span><input type="text" placeholder="0.00" name="monthly_price" class="span2"> <span class="help-inline">Monthly</span>
									</div>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Minimum stay</label>
								<div class="controls">
									<input type="text" name="minimum_stay_number" class="span1">
									<select class="span2" name="minimum_stay_option">
										<option value="1">days</option>
										<option value="2">weeks</option>
										<option value="3">months</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Indoor space</label>
								<div class="controls">
									<div class="input-append">
										<input type="text" name="indoor_space" class="span2"> <span class="add-on">m<sup>2</sup></span>
									</div>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Outdoor space</label>
								<div class="controls">
									<div class="input-append">
										<input type="text" name="outdoor_space" class="span2"> <span class="add-on">m<sup>2</sup></span>
									</div>
								</div>
							</div>
							
						<!-- /content -->
						</div>

						<div class="tab-pane" id="description">
						<!-- content -->
						
							<div class="control-group">
								<label class="control-label">Description</label>
								<div class="controls">
									<textarea rows="6" name="description_pt" class="span7 margin-bottom" placeholder="Portugues"></textarea>
									<textarea rows="6" name="description_es" class="span7 margin-bottom" placeholder="Español"></textarea>
									<textarea rows="6" name="description_en" class="span7" placeholder="English"></textarea>
								</div>
							</div>
						
						<!-- /content -->
						</div>
						<div class="tab-pane" id="features">
						<!-- content -->
						
							<div class="row features">
								<?php foreach ($features as $feature):?>
									<div class="span3"><label class="checkbox"><input type="checkbox" name="feature_id[]" value="<?php echo $feature->id ?>"> <?php echo $feature->name_en ?></label></div>
								<?php endforeach; ?>
							</div>
						
						<!-- /content -->
						</div>
						<div class="tab-pane" id="map">
						<!-- content -->
						
							<div class="control-group">
								<label class="control-label">Address</label>
								<div class="controls">
									<input type="text" id="location" name="location" placeholder="Address" class="span5">
									<a id="map_refresh" href="#" class="btn"><i class="icon-refresh"></i> Refresh map</a>
								</div>
							</div>
						
							<div class="control-group">

								<div class="controls">
									<ul class="thumbnails">
										<li class="">
											<img id="google_maps" src="https://maps.google.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=14&size=550x250&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&sensor=false" alt="" class="thumbnail">
										</li>
									</ul>
								</div>
							</div>
						
						<!-- /content -->
						</div>
						<div class="tab-pane" id="calendar">calendar</div>
						<div class="tab-pane" id="photos">
						<!-- content -->
						
							<div class="well">
								<div class="control-group">
									<label class="control-label">Photo #1</label>
									<div class="controls">
										<input type="file" id="photo_1" name="file1" class="span2">
									</div>
								</div>
							</div>
						
						<!-- /content -->
						</div>
					</div>
					<div class="form-actions">
						<button class="btn btn-primary" type="submit">Save changes</button>
					</div>
				</fieldset>
			<?php echo form_close()?>
		</div>
	</div>
</div>