<div class="page-header">
    <h1>Properties <small>edit</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li><a href="/backend/properties"><i class="icon-th-list"></i> List</a></li>
				<li><a href="/backend/properties/create"><i class="icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab">
				<li><a href="#details">Details</a></li>
				<li><a href="#description">Description</a></li>
				<li><a href="#features">Features</a></li>
				<li><a href="#map">Map</a></li>
				<li><a href="#calendar">Calendar</a></li>
				<li><a href="#photos">Photos</a></li>
			</ul>
 			<?php echo form_open_multipart(current_url(),array('class' => 'form-horizontal'))?>
 				<fieldset>
					<div class="tab-content">
						
						<div class="tab-pane" id="details">
						<!-- content -->
							

							<div class="control-group">
								<label class="control-label">Name</label>
								<div class="controls">
									<input type="text" name="name" class="span6" value="<?php echo $property->name; ?>">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Title</label>
								<div class="controls">
									<input type="text" name="title_pt" value="<?php echo $property->title_pt; ?>" class="span6 margin-bottom" placeholder="Portugues">
									<input type="text" name="title_es" value="<?php echo $property->title_es; ?>" class="span6 margin-bottom" placeholder="Español">
									<input type="text" name="title_en" value="<?php echo $property->title_en; ?>" class="span6" placeholder="English">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Featured</label>
								<div class="controls">
									<label class="radio">
										<input type="radio" value="1" name="featured" <?php if ($property->featured==1): ?>checked="checked"<?php endif; ?>>
											Yes, Show in Homepage
									</label>
									<label class="radio">
										<input type="radio" value="0" name="featured" <?php if ($property->featured==0): ?>checked="checked"<?php endif; ?>>
											No
									</label>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Status</label>
								<div class="controls">
									<label class="radio">
										<input type="radio" <?php if ($property->visible==1): ?>checked="checked"<?php endif; ?> value="1" name="visible">
											Visible
									</label>
									<label class="radio">
										<input type="radio" <?php if ($property->visible==0): ?>checked="checked"<?php endif; ?> value="0" name="visible">
											Hidden
									</label>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Location</label>
								<div class="controls">
									<?php echo $countries; ?>
									<?php echo $cities; ?>
									<?php echo $neighborhoods; ?>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Type</label>
								<div class="controls">
									<label class="radio">
										<input type="radio" <?php if ($property->property_type==1): ?>checked="checked"<?php endif; ?> value="1" name="property_type">
											Apartment
									</label>
									<label class="radio">
										<input type="radio" <?php if ($property->property_type==2): ?>checked="checked"<?php endif; ?> value="2" name="property_type">
											House
									</label>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Bedrooms</label>
								<div class="controls">
									<select name="rooms" class="span1">
										<option <?php if ($property->rooms==1): ?>selected="selected"<?php endif; ?> >1</option>
										<option <?php if ($property->rooms==2): ?>selected="selected"<?php endif; ?> >2</option>
										<option <?php if ($property->rooms==3): ?>selected="selected"<?php endif; ?> >3</option>
										<option <?php if ($property->rooms==4): ?>selected="selected"<?php endif; ?> >4</option>
										<option <?php if ($property->rooms==5): ?>selected="selected"<?php endif; ?> >5</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Bathrooms</label>
								<div class="controls">
									<select name="bathrooms" class="span1">
										<option <?php if ($property->bathrooms==1): ?>selected="selected"<?php endif; ?> >1</option>
										<option <?php if ($property->bathrooms==2): ?>selected="selected"<?php endif; ?> >2</option>
										<option <?php if ($property->bathrooms==3): ?>selected="selected"<?php endif; ?> >3</option>
										<option <?php if ($property->bathrooms==4): ?>selected="selected"<?php endif; ?> >4</option>
										<option <?php if ($property->bathrooms==5): ?>selected="selected"<?php endif; ?> >5</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Max Guests</label>
								<div class="controls">
									<select name="max_guests" class="span1">
										<option <?php if ($property->max_guests==1): ?>selected="selected"<?php endif; ?> >1</option>
										<option <?php if ($property->max_guests==2): ?>selected="selected"<?php endif; ?> >2</option>
										<option <?php if ($property->max_guests==3): ?>selected="selected"<?php endif; ?> >3</option>
										<option <?php if ($property->max_guests==4): ?>selected="selected"<?php endif; ?> >4</option>
										<option <?php if ($property->max_guests==5): ?>selected="selected"<?php endif; ?> >5</option>
										<option <?php if ($property->max_guests==6): ?>selected="selected"<?php endif; ?> >6</option>
										<option <?php if ($property->max_guests==7): ?>selected="selected"<?php endif; ?> >7</option>
										<option <?php if ($property->max_guests==8): ?>selected="selected"<?php endif; ?> >8</option>
										<option <?php if ($property->max_guests==9): ?>selected="selected"<?php endif; ?> >9</option>
										<option <?php if ($property->max_guests==10): ?>selected="selected"<?php endif; ?> >10</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Price</label>
								<div class="controls">
									<div class="input-prepend margin-bottom">
										<span class="add-on">R$</span><input type="text" placeholder="0.00" value="<?php echo $property->daily_price; ?>" name="daily_price" class="span2"> <span class="help-inline">Daily</span>
									</div>
									
									<div class="input-prepend margin-bottom">
										<span class="add-on">R$</span><input type="text" placeholder="0.00" value="<?php echo $property->weekly_price; ?>" name="weekly_price" class="span2"> <span class="help-inline">Weekly</span>
									</div>
									
									<div class="input-prepend margin-bottom">
										<span class="add-on">R$</span><input type="text" placeholder="0.00" value="<?php echo $property->monthly_price; ?>" name="monthly_price" class="span2"> <span class="help-inline">Monthly</span>
									</div>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Minimum stay</label>
								<div class="controls">
									<input type="text" name="minimum_stay_number" class="span1" value="<?php echo $property->minimum_stay_number; ?>">
									<select class="span2" name="minimum_stay_option">
										<option value="1" <?php if ($property->minimum_stay_option==1): ?>selected="selected"<?php endif; ?> >days</option>
										<option value="2" <?php if ($property->minimum_stay_option==2): ?>selected="selected"<?php endif; ?> >weeks</option>
										<option value="3" <?php if ($property->minimum_stay_option==3): ?>selected="selected"<?php endif; ?> >months</option>
									</select>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Indoor space</label>
								<div class="controls">
									<div class="input-append">
										<input type="text" value="<?php echo $property->indoor_space; ?>" name="indoor_space" class="span2"> <span class="add-on">m<sup>2</sup></span>
									</div>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Outdoor space</label>
								<div class="controls">
									<div class="input-append">
										<input type="text" value="<?php echo $property->outdoor_space; ?>" name="outdoor_space" class="span2"> <span class="add-on">m<sup>2</sup></span>
									</div>
								</div>
							</div>
							
						<!-- /content -->
						</div>

						<div class="tab-pane" id="description">
						<!-- content -->
						
							<div class="control-group">
								<label class="control-label">Description</label>
								<div class="controls">
									<textarea rows="6" name="description_pt" class="span7 margin-bottom" placeholder="Portugues"><?php echo $property->description_pt; ?></textarea>
									<textarea rows="6" name="description_es" class="span7 margin-bottom" placeholder="Español"><?php echo $property->description_es; ?></textarea>
									<textarea rows="6" name="description_en" class="span7" placeholder="English"><?php echo $property->description_en; ?></textarea>
								</div>
							</div>
						
						<!-- /content -->
						</div>
						<div class="tab-pane" id="features">
						<!-- content -->
						
							<div class="row features">
								<?php foreach ($features as $feature):?>
									<div class="span3"><label class="checkbox"><input type="checkbox" name="feature_id[]" value="<?php echo $feature->id ?>" <?php if ($feature->checked): ?>checked="checked"<?php endif; ?> > <?php echo $feature->name_en ?></label></div>
								<?php endforeach; ?>
							</div>
						
						<!-- /content -->
						</div>
						<div class="tab-pane" id="map">
						<!-- content -->
						
							<div class="control-group">
								<label class="control-label">Address</label>
								<div class="controls">
									<input type="hidden" name="lat" id="google_lat" value="<?php echo $property->lat; ?>"><input type="hidden" name="lng" id="google_lng" value="<?php echo $property->lng; ?>">
									<input type="text" id="location" name="location" placeholder="Address" class="span5" value="<?php echo $property->location; ?>">
									<a id="map_refresh" href="#" class="btn"><i class="icon-refresh"></i> Refresh map</a>
								</div>
							</div>
						
							<div class="control-group">

								<div class="controls">
									<ul class="thumbnails">
										<li class="">
											<img id="google_maps" src="https://maps.google.com/maps/api/staticmap?center=<?php echo $property->location; ?>&markers=<?php echo $property->location; ?>&zoom=17&size=550x250&maptype=roadmap&sensor=false" alt="" class="thumbnail">
											<div class="thumbnail hide" id="geocoded_map">map</div>
										</li>
									</ul>
								</div>
							</div>
						
						<!-- /content -->
						</div>
						<div class="tab-pane well" id="calendar">
							<div class="control-group">
								<label class="control-label">Date range</label>
								<div class="controls">
									<input type="text" name="from_date" value="" class="span2 from_date" placeholder="From"> <input type="text" name="to_date" value="" class="span2 to_date" placeholder="To"> <select name="availability" class="span2"><option value="1">On Hold</option><option value="2">Unavailable</option></select>
								</div>
								<hr>
								<div class="controls">
									<ul class="unstyled">
										<?php foreach ($availability as $date):?>
										<li class="" title="Check to delete">
											<div class="">
												<label class="checkbox"><input type="checkbox" name="delete_date[]" value="<?php echo $date->id; ?>" > <?php echo $date->from_date; ?> - <?php echo $date->to_date; ?> <?php if ($date->status==1): ?><span class="label label-warning">On Hold</span><?php else: ?><span class="label label-important">Unavailable</span><?php endif; ?></label>
											</div>
										</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
							
						</div>
						<div class="tab-pane" id="photos">
						<!-- content -->
						
							<div class="well">
								<div class="control-group">
									<label class="control-label">Photo #1</label>
									<div class="controls">
										<input type="file" id="photo_1" name="file1" class="span2">
									</div>
									<label class="control-label">Photo #2</label>
									<div class="controls">
										<input type="file" id="photo_2" name="file2" class="span2">
									</div>
									<label class="control-label">Photo #3</label>
									<div class="controls">
										<input type="file" id="photo_3" name="file3" class="span2">
									</div>
									<label class="control-label">Photo #4</label>
									<div class="controls">
										<input type="file" id="photo_4" name="file4" class="span2">
									</div>
									<label class="control-label">Photo #5</label>
									<div class="controls">
										<input type="file" id="photo_5" name="file5" class="span2">
									</div>
								</div>
							</div>
							
							<ul class="thumbnails">
								<?php foreach ($photos as $photo):?>
								<li class="span3">
									<div class="thumbnail">
										<img src="/upload/properties/prev/<?php echo $photo->id; ?>.jpg" width="260" height="180" alt="">
									</div>
									<div><label class="checkbox"><input type="checkbox" name="delete_photo[]" value="<?php echo $photo->id; ?>" > Delete</label> <label class="radio"><input type="radio" value="<?php echo $photo->id; ?>" name="default" <?php if ($photo->default): ?>checked="checked"<?php endif; ?>>Default</label></div>
								</li>
								<?php endforeach; ?>
							</ul>
						
						<!-- /content -->
						</div>
					</div>
					<div class="form-actions">
						<button class="btn btn-primary" type="submit">Save changes</button>
					</div>
				</fieldset>
			<?php echo form_close()?>
		</div>
	</div>
</div>