<div class="page-header">
    <h1>Properties <small>list</small></h1>
  </div>
  
<div class="row">
	<div class="span3">
		<!--Sidebar content-->
		<div style="" class="well sidebar">
			<ul class="nav nav-list">
				<li class="nav-header">Actions</li>
				<li class="active"><a href="/backend/properties"><i class="icon-white icon-th-list"></i> List</a></li>
				<li><a href="/backend/properties/create"><i class="icon-file"></i> Create</a></li>
			</ul>
      </div>
		<!--/Sidebar content-->
	</div>
	<div class="span9">
		<table class="table table-stripped">
			<thead>
				<tr>
					<th># ID</th>
					<th>Name</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($properties as $property):?>
				<tr>
					<td><?php echo $property->id?></td>
					<td><?php echo $property->name?></td>
					<td><a href="/backend/properties/edit/<?php echo $property->id?>" class="btn btn-mini">edit</a> <a href="/backend/properties/delete/<?php echo $property->id?>" class="btn btn-mini btn-danger">delete</a> <a href="/properties/view/<?php echo $property->id?>" target="_blank" class="btn btn-mini btn-info"><i class="icon-eye-open icon-white"></i> view online</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>