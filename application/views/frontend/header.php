<div class="inner table">
	<div class="row">
		<div class="logo col">
			<?php if (isset($citySelectedId)): ?>
				<?php switch($citySelectedId): 
					      case 1: ?>
					      	<a href="/brasil">
								<img src="/assets/img/logo_sp.png" width="250" height="44" alt="south4rent-logo">
							</a>
							<?php break;?>
					<?php case 5: ?>
							<a href="/chile">
								<img src="/assets/img/S4R-logo_sch.png" width="250" height="44" alt="south4rent-logo">
							</a>			
							<?php break;?>
					<?php default: ?>
							<a href="/">
								<img src="/assets/img/logo_simple.png" alt="south4rent-logo">
							</a>
							<?php break;?>	
				<?php endswitch;?>
			<?php else: ?>
				<a href="/">
					<img src="/assets/img/logo_simple.png" alt="south4rent-logo">
				</a>
			<?php endif; ?>
		</div>
		<div class="nav col">
			<ol>
				<li><a href="/why-south4rent"><?php echo lang('header_why') ?></a></li>
				<li class="spacer"><img src="/assets/img/header_spacer.png" width="7" height="71" style="vertical-align:middle" alt=""></li>
				<li><a href="/how-it-works"><?php echo lang('header_how') ?></a></li>
				<li class="spacer"><img src="/assets/img/header_spacer.png" width="7" height="71" style="vertical-align:middle" alt=""></li>
				<li><a href="/contact-us"><?php echo lang('header_contact') ?></a></li>
				<li class="spacer"><img src="/assets/img/header_spacer.png" width="7" height="71" style="vertical-align:middle" alt=""></li>
				<li class="lang">
					<div class="options">
						<a href="/languages/set/spanish"><img width="24" height="24" src="/assets/img/flag-es.png" style="vertical-align:middle" alt=""></a>
						<a href="/languages/set/english"><img width="24" height="24" src="/assets/img/flag-en.png" style="vertical-align:middle" alt=""></a>
						<a href="/languages/set/portuguese"><img width="24" height="24" src="/assets/img/flag-pt.png" style="vertical-align:middle" alt=""></a>
					</div>
				</li>
				<li class="list"><a href="/list-your-property"><?php echo lang('header_list_your_property') ?></a></li>
				<li>
			
				</li>
			</ol>
		</div>
	</div>
</div>	