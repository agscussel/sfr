<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title ?></title>
	<meta charset="utf-8">
	<!--meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"--> 
	<link href="/assets/css/reset.css" rel="stylesheet" type="text/css" media="screen">
	<link href="/assets/css/frontend.css?device=web" rel="stylesheet" type="text/css" media="screen"> 
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/assets/js/frontend.js"></script>
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-817079-11']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!--script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script-->
</head>
<body class="<?php echo $this->router->fetch_class(); ?>-<?php echo $this->router->fetch_method(); ?>">

	<div id="container">
		<div id="header">
			<?php echo $header?>
		</div>
		<div id="home"><a href="/welcome/homepage"><img src="/assets/img/home.png" width="24" height="24" style="border:0" alt=""></a></div>
		<div id="content">
			<div class="inner bg3">
				<?php echo $content?>
			</div>
		</div>
		<div id="footer">
			<?php echo $footer?>
		</div>
	</div>

<!--script type="text/javascript">stLight.options({publisher: "6920f995-f5e2-4875-a61b-eccaed9493d8"});</script>
<script>
var options={ "publisher": "6920f995-f5e2-4875-a61b-eccaed9493d8", "position": "right", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "linkedin", "email"]}};
var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
</script-->

</body>
</html>