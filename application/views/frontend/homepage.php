<div class="city-selection">
	<div class="city sanpablo-brasil"></div>
	<div class="city santiago-chile"></div>
</div>

<div class="home-boxes table">
    <div class="row">
        <div class="col why"><p><?php echo lang('homepage_box_why') ?></p></div>
        <div class="col list"><p><?php echo lang('homepage_box_list') ?></p></div>
        <div class="col contact-us"><p><?php echo lang('homepage_box_contact_us') ?></p></div>
    </div>
</div>