<div class="page-title"><?php echo lang('contact_title'); ?></div>
<div class="page-content">
	<? if ($confirmation): ?>
	<div class="message success">
		<?php echo lang('contact_confirmation'); ?>
	</div>
	<? endif; ?>
	<?php echo form_open(current_url()) ?>
	<div class="table">
		<div class="row">
			<div class="col">
				<div class="form">
					<div class="row-form">
						<p><?php echo lang('contact_form_first_last'); ?></p>
						<input name="first_last" type="text" value="">
					</div>
					<div class="row-form">
						<p>Email</p>
						<input name="email" type="text" value="">
					</div>
					<div class="row-form">
						<p><?php echo lang('contact_form_telephone'); ?></p>
						<input name="telephone" type="text" value="">
					</div>
					<div class="row-form">
						<p><?php echo lang('contact_form_comments'); ?></p>
						<textarea name="comments"></textarea>
					</div>
					<div class="row-form">
						<button type="submit"><?php echo lang('contact_form_send'); ?></button>
					</div>
				</div>
			</div>
				
			<div class="col information bigger">
				<p>p</p>
			</div>
		</div>
	</div>
	<?php echo form_close()?>
</div>