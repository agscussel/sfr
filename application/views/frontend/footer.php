<div class="inner">
	<ol>
		<li><a href="/about-us"><?php echo lang('footer_about_us') ?></a></li>
		<!--li><a href="/press"><?php echo lang('footer_press') ?></a></li-->
		<li><a href="/work-with-us"><?php echo lang('footer_jobs') ?></a></li>
		<li><a href="/terms-and-conditions"><?php echo lang('footer_terms_and_conditions') ?></a></li>
		<li><a href="/privacy-policy"><?php echo lang('footer_privacy_policy') ?></a></li>
		<li><a href="/contact-us"><?php echo lang('footer_contact_us') ?></a></li>
		<li>
			<a href="http://www.twitter.com/south4rent" target="_blank"><img src="/assets/img/twitter.png" width="36" height="36" align="absmiddle"></a>
			<a href="http://www.facebook.com/#!/pages/South4Rent/345712062151978?fref=ts " target="_blank"><img src="/assets/img/like_<?php echo $this->session->userdata('language'); ?>.png" width="108" height="36" align="absmiddle"></a>		
		</li>
	</ol>
</div>
<div class="address">
	<?php if (isset($countrySelectedId)): ?>
		<?php switch($countrySelectedId): 
			      case 1: ?>
			      	<p>SOUTH4RENT  |  Rua Zacarias de Gois 707, São Paulo, SP  | Tel. (55 11) 3280 9231  | e-mail info@south4rent.com</p>
					<?php break;?>
			<?php case 4: ?>
					<p>SOUTH4RENT | Robles 12.495, Santiago de Chile | Tel. (56 9) 6575 5878 o (56 9) 8221 7574  | e-mail info@south4rent.com</p>			
					<?php break;?>
			<?php default: ?>
					<p>SOUTH4RENT  |  Rua Zacarias de Gois 707, São Paulo, SP  | Tel. (55 11) 3280 9231  | e-mail info@south4rent.com</p>
					<?php break;?>	
		<?php endswitch;?>
	<?php else: ?>
		<p>SOUTH4RENT  |  e-mail info@south4rent.com</p>
	<?php endif; ?>
</div>