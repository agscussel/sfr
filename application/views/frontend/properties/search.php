<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var geocoder, map, bounds, zoomBounds;
var locations = [
	<?php foreach ($properties as $property):?>
	<?php //$location = $property->address_street.' '.$property->address_number.', '.$property->city_name.', '.$property->neigborhood_name.', '.$property->country_name; ?>
	['<?php echo $property->id; ?>','<?php echo $property->lat; ?>','<?php echo $property->lng; ?>'],
	<?php endforeach; ?>
];

function initialize() {
	geocoder = new google.maps.Geocoder();
	var myOptions = {
		//zoom: 17,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	
	bounds = new google.maps.LatLngBounds();
	map = new google.maps.Map(document.getElementById("search_map"), myOptions);
	
	for (i = 0; i < locations.length; i++) {
		pin(locations[i][1],locations[i][2],i);
	}

}

function codeAddress(address,i) {
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: map, 
				position: results[0].geometry.location,
				animation: google.maps.Animation.DROP,
			});
			bounds.extend(results[0].geometry.location);
			map.fitBounds(bounds);
			
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					$(location).attr('href','/properties/view/'+locations[i][0]);
				}
			})(marker, i));
			
		} else {
			console.log("Geocode was not successful for the following reason: " + status);
 		}
	});
}

$(function(){
	initialize();
	
	$('.sort select').change(function() {
		if($(this).val()>0){
			var url = '<?php echo $link; ?>' + $(this).val();    
			$(location).attr('href',url);
		}
	});	
});
</script>
<div class="search-box table">
	<div class="row">
		<div class="title col">
			<span><?php echo lang('homepage_find_the_best_place') ?></span>
		</div>
		<div class="fields col">
			<ul>
				<li class="param neighborhood arrow"><?php echo $where_dropdown; ?></li>
				<li class="param check-in calendar"><?php echo $in_input; ?></li>
				<li class="param check-out calendar"><?php echo $out_input; ?></li>
				<li class="param rooms arrow"><?php echo $rooms_dropdown; ?></li>
				<li class="search">Ok</li>
			</ul>	
		</div>
	</div>
</div>

<div class="page-content">
	<?php if ($properties): ?>
	<div class="search-results table">
		<div class="row">
			<div class="col properties">
				<div class="table">
				<?php foreach ($properties as $property):?>
					<div class="row">
						<div class="property" property="<?php echo $property->id; ?>">
							<div class="col image"><img class="shadow-outer" src="/upload/properties/prev/<?php echo $property->photo; ?>.jpg" width="260" height="180" alt=""></div>
							<div class="col description">
								<a href="/properties/view/<?php echo $property->id; ?>"><?php echo $property->name; ?></a>
								<ol>
									<li><span><?php echo lang('properties_search_neighborhood') ?>:</span> <?php echo $property->neigborhood_name; ?></li>
									<li><span><?php echo lang('properties_search_rooms') ?>:</span> <?php echo $property->rooms; ?></li>
									<li><span><?php echo lang('properties_search_bathrooms') ?>:</span> <?php echo $property->bathrooms; ?></li>
								</ol>
								<ol>
									<li><span>mts2</span><?php echo $property->indoor_space; ?></li>
								</ol>
								<ol class="price">
									<!--li><span><?php echo lang('properties_search_weekly') ?>:</span> R$ <?php echo $property->weekly_price; ?></li-->
									<li><span><?php echo lang('properties_search_monthly') ?>:</span> <?php echo currencySimbol($property->country_id); ?> <?php echo number_format($property->monthly_price, 0, ',', '.'); ?></li>
								</ol>
								<ol>
									<li class="yellow-box rounded"><?php echo lang('properties_search_view_property') ?></li>
								</ol>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
				
			<?php if ($pagination): ?>
				<div class="pagination"><?php echo $pagination; ?></div>
			<?php endif; ?>	
			
			</div>
			<div class="col" style="vertical-align:top">
				<div class="sort">
				<?php echo lang('properties_search_sort_by') ?>: <select>
					<option value="0"></option>
					<option value="1" <?php if($sort=='1'){echo 'selected="selected"';} ?>><?php echo lang('properties_search_sort_by_max_price') ?></option>
					<option value="2" <?php if($sort=='2'){echo 'selected="selected"';} ?>><?php echo lang('properties_search_sort_by_min_price') ?></option>
					<option value="3" <?php if($sort=='3'){echo 'selected="selected"';} ?>><?php echo lang('properties_search_sort_by_area') ?></option>
					<option value="4" <?php if($sort=='4'){echo 'selected="selected"';} ?>><?php echo lang('properties_search_sort_by_space') ?></option>
				</select>
				</div>
				<div id="search_map" class="shadow-outer"></div>
			</div>
		</div>
	</div>
	<?php else: ?>
	<h1><?php echo lang('properties_search_no_results') ?></h1>
	<?php endif; ?>
</div>