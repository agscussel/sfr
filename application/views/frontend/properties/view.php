<?php //$location = $property->address_street.' '.$property->address_number.', '.$property->city_name.', '.$property->neigborhood_name.', '.$property->country_name; ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var geocoder, map;	

function initialize() {
	console.log('data');

	var myOptions = {
		zoom: 17,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	
	geocoder = new google.maps.Geocoder();
	map = new google.maps.Map(document.getElementById('map'), myOptions);
	geocoder.geocode( { 'address': '<?php echo $property->location; ?>'}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: map, 
				position: results[0].geometry.location,
				animation: google.maps.Animation.DROP
			});
			console.log(status);
		} else {
			//alert("Geocode was not successful for the following reason: " + status);
 		}
	});
	
	google.maps.event.trigger(map, 'resize');
}
 
 
var bad_dates =[
	<?php foreach ($availability as $date):?>
	['<?php echo $date[0]; ?>','<?php echo $date[1]; ?>','<?php echo $date[2]; ?>','<?php echo $date[3]; ?>'],
	<?php endforeach; ?>
];

$(function() {

	$("#calendar div.render").datepicker({ 
		beforeShowDay: set_dates,
		dateFormat: 'yy-mm-dd',
		minDate: 0,
		prevText: '<?php echo lang('global_prev'); ?>',
		nextText: '<?php echo lang('global_next'); ?>'
	});

	function set_dates(date) {
		var y,m,d;
		for (var j = 0; j < bad_dates.length; j++) {
			y = date.getFullYear();
			m = date.getMonth();
			d = date.getDate();
			if (y == bad_dates[j][0] && m == bad_dates[j][1] - 1 && d == bad_dates[j][2]) {
				return [true, 'status'+bad_dates[j][3]];
			}
		}
		return [true, ''];
	}
});
</script>
<div class="property-information">
	<div class="nav">
		<img alt="" src="/assets/img/icons/orange/calendar_alt_stroke_12x12.png"><span class="calendar_select"><?php echo lang('properties_view_calendar'); ?></span> <img alt="" src="/assets/img/icons/orange/camera_12x12.png"><span class="photos_select"><?php echo lang('properties_view_photos'); ?></span> <img alt="" src="/assets/img/icons/orange/map_pin_fill_12x12.png"><span class="map_select"><?php echo lang('properties_view_map'); ?></span> <!-- <img alt="" src="/assets/img/icons/orange/eye_12x9.png"><span class="currency_select"><?php echo lang('properties_view_currency'); ?></span> <span class="currency_rs rounded">R$</span> <span class="currency_usd rounded">USD</span> -->
	</div>
	<h1><?php echo $property->name; ?></h1>
	<h2><?php echo $property->title; ?></h2>
	<a class="back" href="Javascript:parent.history.back(); ">&larr; <?php echo lang('properties_view_back'); ?></a>
	<div class="table data" style="margin-top:10px">
		<div class="row">
			<div class="col left">
				<div id="calendar" class="property-calendar">
					<div class="render"></div>
					<div class="info">
						<div class="available rounded"><?php echo lang('properties_view_available'); ?></div>
						<div class="hold rounded"><?php echo lang('properties_view_hold'); ?></div>
						<div class="unavailable rounded"><?php echo lang('properties_view_unavailable'); ?></div>
					</div>
				</div> 
				<div id="map" class="property-map">
					loading map...
				</div> 
				<div id="photos" class="property-photos">
					
					<div class="full">
						<div class="arrow prev">prev</div>
						<div class="arrow next">next</div>
					<img alt="" src="/upload/properties/full/<?php echo $default; ?>.jpg" data-actual="1">
					</div>
					<div class="thumbs">
						<ul>
							<?php $order = 1; ?>
							<?php foreach ($photos as $photo):?>
							<li data-order="<?php echo $order;?>" data-photo="<?php echo $photo->id; ?>"><img alt="" src="/upload/properties/square/<?php echo $photo->id; ?>.jpg"></li>
							<?php $order ++; ?>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="right col">
				<div class="title"><?php echo lang('properties_view_general_features'); ?></div>
				<ul>
					<li class="alter"><?php echo lang('properties_view_name'); ?>: <?php echo $property->name; ?></li>
					<li><?php echo lang('properties_view_destination'); ?>: <?php echo $property->neigborhood_name; ?>, <?php echo $property->city_name; ?></li>
					<li class="alter"><?php echo lang('properties_view_indoor_space'); ?>: <?php echo $property->indoor_space; ?> mt2 <small>(<?php echo $property->indoor_space*3.2; ?> sq2)</small></li>
					<li><?php echo lang('properties_view_outdoor_space'); ?>: <?php echo $property->outdoor_space; ?> mt2 (<?php echo $property->outdoor_space*3.2; ?> sq2)</li>
					<li class="alter"><?php echo lang('properties_view_bedrooms'); ?>: <?php echo $property->rooms; ?></li>
					<li><?php echo lang('properties_view_bathrooms'); ?>: <?php echo $property->bathrooms; ?></li>
					<li class="alter"><?php echo lang('properties_view_minimum_stay'); ?>: <?php echo $property->minimum_stay_number; ?> <?php if($property->minimum_stay_option==1) { echo lang('properties_view_minimum_stay_days'); } elseif($property->minimum_stay_option==2) { echo lang('properties_view_minimum_stay_weeks'); } else {echo lang('properties_view_minimum_stay_months');}?></li>
					<li><?php echo lang('properties_view_max_guests'); ?>: <?php echo $property->max_guests; ?></li>
					<li class="alter"><?php echo lang('properties_view_weekly'); ?>: <span class="currency_convert_rs"><?php echo $currency_simbol ?> <?php echo number_format($property->weekly_price, 0, ',', '.'); ?></span></li>
					<li><?php echo lang('properties_view_monthly'); ?>: <span class="currency_convert_rs"></span><?php echo $currency_simbol ?> <?php echo number_format($property->monthly_price, 0, ',', '.'); ?></span></li>
				</ul>
				<a href="/properties/contact/<?php echo $property->id; ?>" class="reserve rounded text-shadow"><?php echo lang('properties_view_reserve_place'); ?></a>
			</div>
		</div>
	</div>
</div>

<div class="property-description">
	<div class="title"><?php echo lang('properties_view_description'); ?></div>
	<div class="content">
	<?php echo bbcode(nl2br($property->description)); ?>
	</div>
</div>

<div class="property-features">
	<div class="title"><?php echo lang('properties_view_features'); ?></div>
	<div class="content">
		<ul>
			<?php foreach ($features as $feature):?>
			<li><img alt="" width="35" height="36" src="/assets/img/features/<?php echo $feature->id; ?>.jpg" style="vertical-align:middle"> <?php echo $feature->name; ?></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>