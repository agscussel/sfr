<div class="page-title"><?php echo lang('properties_create_title'); ?></div>
<div class="page-content">
	<?php if ($confirmation): ?>
	<div class="message success">
		<?php echo lang('properties_create_confirmation'); ?>
	</div>
	<?php endif; ?>
	
	<?php echo form_open(current_url()) ?>
	<div class="form pull-right rounded highlight-box" style="margin-left:40px; text-align:left">
		<div class="row-form">
			<p><?php echo lang('properties_create_form_first_last'); ?></p>
			<input name="first_last" type="text" value="">
		</div>
		<div class="row-form">
			<p>Email</p>
			<input name="email" type="text" value="">
		</div>
		<div class="row-form">
			<p><?php echo lang('properties_create_form_telephone'); ?></p>
			<input name="telephone" type="text" value="">
		</div>
		<div class="row-form">
			<button type="submit"><?php echo lang('properties_create_form_send'); ?></button>
		</div>
	</div>
	<?php echo form_close()?>
	
	<div class="information">
		<?php echo nl2br(bbcode($information)); ?>
	</div>
	
</div>