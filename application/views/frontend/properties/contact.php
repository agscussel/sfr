<script>
$(function() {
	$('#contact1').submit(function() {
		if($('#first_last').val() == '' || $('#email').val() == '' || $('#telephone').val() == '' || $('#search_in').val() == '' || $('#search_out').val() == '' || $('#how').val() == ''){
		alert('<?php echo lang('properties_contact_required_error'); ?>');
		return false;
		}
	});
});
</script>
<div class="page-title"><?php echo lang('properties_contact_title'); ?></div>
<div class="page-content">
	<div class=""><?php echo nl2br(bbcode($information)) ?></div>
	<?php if ($confirmation): ?>
	<div class="message success">
		<?php echo lang('properties_contact_confirmation'); ?>
	</div>
	<?php endif; ?>
	<?php echo form_open(current_url(),'id="contact1"') ?>
	<div class="table" style="margin-top:20px; width:100%">
		<div class="row">
			<div class="col">
				<div class="form">
					<div class="row-form">
						<p><?php echo lang('properties_contact_form_first_last'); ?> *</p>
						<input id="first_last" name="first_last" type="text" value="">
					</div>
					<div class="row-form">
						<p>Email *</p>
						<input id="email" name="email" type="text" value="">
					</div>
					<div class="row-form">
						<p><?php echo lang('properties_contact_form_telephone'); ?> *</p>
						<input id="telephone" name="telephone" type="text" value="">
					</div>
					<div class="row-form">
						<p><strong><?php echo lang('properties_contact_select_your_dates'); ?></strong> *</p>
						<input id="search_in" name="check_in" type="text" value="" placeholder="CHECK IN" style="width:168px"> <input id="search_out" name="check_out" type="text" value="" placeholder="CHECK OUT" style="width:168px; margin-left:4px">
					</div>
					<div class="row-form">
						<p><?php echo lang('properties_contact_how_did_you_found'); ?> *</p>
						<select id="how" name="how">
							<option value=""></option>
							<option>Google</option>
							<option><?php echo lang('properties_contact_how_did_you_found_option1'); ?></option>
							<option><?php echo lang('properties_contact_how_did_you_found_option2'); ?></option>
						</select>					
					</div>
					<div class="row-form">
						<p><?php echo lang('properties_contact_number_of_guests'); ?></p>
						<select name="guests">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</select>					
					</div>
					<div class="row-form">
						<p><?php echo lang('properties_contact_children'); ?></p>
						<label><input type="radio" name="children" value="YES"> <?php echo lang('properties_contact_yes'); ?></label> <label><input type="radio" name="children" value="NO" checked="checked"> <?php echo lang('properties_contact_no'); ?></label>
					</div>
					<div class="row-form">
						<p><?php echo lang('properties_contact_form_comments'); ?></p>
						<textarea name="comments"></textarea>
					</div>
					<div class="row-form">
						<button type="submit"><?php echo lang('properties_contact_form_send'); ?></button>
					</div>
					<div class="row-form">
					* = <?php echo lang('properties_contact_required'); ?>
					</div>
				</div>
			</div>
				
			<div class="col information" style="vertical-align:top">
				<div class="highlight-box rounded">
					<p><strong><?php echo $property->name; ?></strong></p><br>
					<img class="rounded shadow-outer" src="/upload/properties/prev/<?php echo $property->photo; ?>.jpg"><br>
					<?php echo $property->title; ?>
					<input type="hidden" value="<?php echo $property->id; ?>" name="property_id">
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close()?>
</div>