<div class="home-featured">
	<div class="arrow prev">prev</div>
	<div class="arrow next">next</div>
	<?php foreach ($featured as $property):?>
	<div property="<?php echo $property->id; ?>" class="property" style="background:url('/upload/properties/slide/<?php echo $property->photo; ?>.jpg') center center">
		<div class="info">
			<h1><?php echo $property->name; ?></h1>
			<p><?php echo $property->neigborhood_name; ?> / <?php echo $property->rooms; ?> <?php if($property->rooms==1) { echo lang('homepage_search_rooms_singular'); } else {echo lang('homepage_search_rooms_plural');} ?></p>
		</div>
	</div>
	<?php endforeach; ?>
</div>

<div class="search-box table">
	<div class="row">
		<div class="title col">
			<span><?php echo lang('homepage_find_the_best_place') ?></span>
		</div>
		<div class="fields col">
			<ul>
				<li class="param neighborhood arrow"><?php echo $where_dropdown; ?></li>
				<li class="param check-in calendar"><?php echo $in_input; ?></li>
				<li class="param check-out calendar"><?php echo $out_input; ?></li>
				<li class="param rooms arrow"><?php echo $rooms_dropdown; ?></li>
				<li class="search">Ok</li>
			</ul>	
		</div>
	</div>
</div>

<div class="home-boxes table">
    <div class="row">
        <div class="col why"><p><?php echo lang('homepage_box_why') ?></p></div>
        <div class="col list"><p><?php echo lang('homepage_box_list') ?></p></div>
        <div class="col cityguide chile"><p><?php echo lang('homepage_box_cityguide') ?></p></div>
    </div>
</div>

<div style="text-align:center">
	<img src="/assets/img/spacer.png">
</div>

<div class="home-press">
	<img src="/assets/img/footer_santiago_2.png" width="140" style="margin-right: 150px;">
	<img src="/assets/img/footer_santiago_1.jpg" width="200" style="margin-bottom: 10px;">
</div>