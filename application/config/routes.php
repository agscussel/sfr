<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller']       	= "welcome";
$route['404_override']				= '';

$route['chile']						= "/welcome/chile_homepage";
$route['brasil']					= "/welcome/brasil_homepage";

$route['why-south4rent'] 			= "/pages/index/1";
$route['how-it-works'] 				= "/pages/index/2";
$route['about-us']					= "/pages/index/3";
$route['press']						= "/pages/index/4";
$route['work-with-us']				= "/pages/index/5";
$route['terms-and-conditions']		= "/pages/index/6";
$route['privacy-policy']			= "/pages/index/7";

$route['contact-us']				= "/welcome/contact";
$route['contact-us/(:any)']			= "/welcome/contact/$1";
$route['cityguide']					= "/pages/index/10";
$route['cityguide/santiago']		= "/pages/index/14";
$route['cityguide/saopaulo']		= "/pages/index/10";
$route['tell-a-friend']				= "/welcome/tell_a_friend";

$route['list-your-property']		= "/properties/create";

$route['backend']		            = "/backend/accounts/signin";


/* End of file routes.php */
/* Location: ./application/config/routes.php */